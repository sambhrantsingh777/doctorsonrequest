package com.doctorsonrequest.savatarr;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.doctorsonrequest.savatarr.R;
import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.doctorsonrequest.savatarr.SplashActivity.API_URL;
import static com.doctorsonrequest.savatarr.SplashActivity.MyPREFERENCES;

public class DoctorProfileActivity extends AppCompatActivity implements View.OnClickListener {
    private TextView txt_edit_password,txt_edit_phone,txt_edit_email,txt_edit_name;
    private EditText ed_name,ed_email,ed_phone,ed_password,ed_description;
    private TextView txt_name,txt_email,txt_phone,txt_password,txt_desc,txt_edit_dis,txt_description;
    private RelativeLayout rl_name,rl_email,rl_phone,rl_password;
    private Button btn_update;
    private ImageView go_back, profile_pic;
    private String name,email,password,mobile,img,description;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_profile);
        fetchProfileInfo(this);

        go_back =  findViewById(R.id.go_back);
        btn_update =  findViewById(R.id.button_submit);
        txt_edit_password =  findViewById(R.id.txt_edit_password);
        txt_edit_phone=  findViewById(R.id.txt_edit_phone);
        txt_edit_email =  findViewById(R.id.txt_edit_email);
        txt_edit_name =  findViewById(R.id.txt_edit_name);
        ed_name =  findViewById(R.id.ed_name);
        ed_email=  findViewById(R.id.ed_email);
        ed_phone =  findViewById(R.id.ed_phone);
        ed_password=  findViewById(R.id.ed_password);
        txt_name=  findViewById(R.id.txt_name);
        txt_email=  findViewById(R.id.txt_email);
        txt_phone =  findViewById(R.id.txt_phone);
        txt_password =  findViewById(R.id.txt_password);
        rl_name=  findViewById(R.id.rl_name);
        rl_email=  findViewById(R.id.rl_email);
        rl_phone=  findViewById(R.id.rl_phone);
        rl_password =  findViewById(R.id.rl_password);
        ed_description = findViewById(R.id.ed_description);
        txt_desc = findViewById(R.id.txt_desc);
        txt_edit_dis = findViewById(R.id.txt_edit_dis);
        txt_description = findViewById(R.id.txt_desc);
        profile_pic = findViewById(R.id.d_prof_pic);

        txt_edit_dis.setOnClickListener(this);
        txt_edit_name.setOnClickListener(this);
        txt_edit_email.setOnClickListener(this);
        txt_edit_phone.setOnClickListener(this);
        btn_update.setOnClickListener(this);
        txt_edit_password.setOnClickListener(this);
        go_back.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.txt_edit_password:
                rl_password.setVisibility(View.GONE);
                ed_password.setVisibility(View.VISIBLE);

                break;
            case R.id.txt_edit_phone:
                rl_phone.setVisibility(View.GONE);
                ed_phone.setVisibility(View.VISIBLE);

                break;
            case R.id.txt_edit_email:
                rl_email.setVisibility(View.GONE);
                ed_email.setVisibility(View.VISIBLE);
                break;
            case R.id.txt_edit_name:
                rl_name.setVisibility(View.GONE);
                ed_name.setVisibility(View.VISIBLE);
                ed_name.setText(name);
                break;
            case R.id.txt_edit_dis:
                txt_desc.setVisibility(View.GONE);
                txt_edit_dis.setVisibility(View.GONE);
                ed_description.setVisibility(View.VISIBLE);
                ed_description.setText(description);
                break;
            case R.id.button_submit:
                rl_password.setVisibility(View.VISIBLE);
                ed_password.setVisibility(View.GONE);
                rl_phone.setVisibility(View.VISIBLE);
                ed_phone.setVisibility(View.GONE);
                rl_email.setVisibility(View.VISIBLE);
                ed_email.setVisibility(View.GONE);
                rl_name.setVisibility(View.VISIBLE);
                ed_name.setVisibility(View.GONE);
                txt_desc.setVisibility(View.VISIBLE);
                txt_edit_dis.setVisibility(View.VISIBLE);
                ed_description.setVisibility(View.GONE);
                editInfo(v, getApplicationContext(), String.valueOf(ed_name.getText()),String.valueOf(ed_email.getText()),String.valueOf(ed_phone.getText()),String.valueOf(ed_password.getText()));
                break;
            case R.id.go_back:
                finish();
                break;
        }

    }

    public void fetchProfileInfo(Context context) {
        RequestQueue queue = Volley.newRequestQueue(context);
        String url = API_URL + "getUserData.php";
        // Request a string response from the provided URL.
        StringRequest sr = new StringRequest(Request.Method.POST,url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                JSONObject jObject = null;
                JSONObject jData = null;
                try {
                    jObject = new JSONObject(response);
                    String status = jObject.getString("status");
                    String data = jObject.getString("data");
                    if(status.equals("Success")){
                        jData = new JSONObject(data);
                         name = jData.getString("name");
                         email = jData.getString("email");
                         mobile = jData.getString("phone");
                         description = jData.getString("description");
                         img = jData.getString("profile_pic");
                         password = jData.getString("password");
                        txt_name.setText(name);
                        txt_email.setText(email);
                        txt_phone.setText(mobile);
                        txt_description.setText(description);
                        Picasso.get().load(API_URL+"images/"+img).into(profile_pic);
                    }
                }
                catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("savatarr_errorOnCall", String.valueOf(error));
            }
        }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                SharedPreferences sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                String user_name = sharedpreferences.getString("user_name", "NULL");
                params.put("username", user_name);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/x-www-form-urlencoded");
                return params;
            }
        };
        queue.add(sr);
    }

    public void editInfo(final View v, Context context, final String edname, final String edemail, final String edphone, final String edpassword) {
        RequestQueue queue = Volley.newRequestQueue(context);
        String url = API_URL + "editInfoDoc.php";
        if(!edname.equals("")||!edemail.equals("")||!edphone.equals("")||!edpassword.equals("")){

            // Request a string response from the provided URL.
            StringRequest sr = new StringRequest(Request.Method.POST,url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    JSONObject jObject;
                    try {
                        jObject = new JSONObject(response);
                        String status = jObject.getString("status");
                        String mssg = jObject.getString("mssg");
                        if(status.equals("Success")){
                            Snackbar snackbar = Snackbar
                                    .make(v, "Profile Data updated!", Snackbar.LENGTH_LONG);
                            snackbar.show();
                            if(!edname.equals("")){
                                txt_name.setText(edname);
                            }
                            if(!edemail.equals("")) {
                                txt_email.setText(edemail);
                            }
                            if(!edphone.equals("")){
                                txt_phone.setText(edphone);
                            }

                        }
                        else{
                            Snackbar snackbar = Snackbar
                                    .make(v, mssg, Snackbar.LENGTH_LONG);
                            snackbar.show();
                        }
                    }
                    catch (JSONException e){
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d("savatarr_errorOnCall", String.valueOf(error));
                    Snackbar snackbar = Snackbar
                            .make(v, "Some error occurred while updating profile. Check your connection.", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
            }){
                @Override
                protected Map<String,String> getParams(){
                    Map<String,String> params = new HashMap<String, String>();
                    SharedPreferences sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                    String user_name = sharedpreferences.getString("user_name", "NULL");
                    params.put("user_name", user_name);
                    params.put("name", edname);
                    params.put("email", edemail);
                    params.put("phone", edphone);
                    params.put("password", edpassword);
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String,String> params = new HashMap<String, String>();
                    params.put("Content-Type","application/x-www-form-urlencoded");
                    return params;
                }
            };
            queue.add(sr);
        }
        else{
            Snackbar snackbar = Snackbar
                    .make(v, "Nothing to update!", Snackbar.LENGTH_LONG);
            snackbar.show();
        }
    }
}

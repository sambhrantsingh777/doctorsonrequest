package com.doctorsonrequest.savatarr;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.doctorsonrequest.savatarr.utils.SnackbarManager;
import com.doctorsonrequest.savatarr.R;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import static com.doctorsonrequest.savatarr.SplashActivity.API_URL;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener {
    private Button register;
    private ImageView back;
    private TextView userlogin;
    public static final String MyPREFERENCES = "userData" ;
    private ProgressBar loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        register = (Button)findViewById(R.id.button_register);
        userlogin = (TextView)findViewById(R.id.text_login);
        ProgressBar loading = (ProgressBar)findViewById(R.id.progressBar);

        register.setOnClickListener(this);

        String text = userlogin.getText().toString();
        SpannableString ss = new SpannableString(text);

        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),LoginActivity.class));
                finish();
//
            }
            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(getResources().getColor(R.color.colorPrimary));
                ds.setUnderlineText(false);
            }
        };

        ss.setSpan(clickableSpan, 19,34,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        userlogin.setText(ss);
        userlogin.setMovementMethod(LinkMovementMethod.getInstance());
        userlogin.setHighlightColor(Color.TRANSPARENT);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button_register:

                if(validate()){
                    registerRequest(v);
                }
                break;
        }
    }

    public  void  registerRequest(final View v) {
        ProgressBar loading = (ProgressBar)findViewById(R.id.progressBar);
        EditText firstName = (EditText)findViewById(R.id.firstName);
        EditText lastName = (EditText)findViewById(R.id.lastName);
        EditText reg_email = (EditText)findViewById(R.id.reg_email);
        EditText reg_phone = (EditText)findViewById(R.id.reg_phone);
        EditText reg_pwd = (EditText)findViewById(R.id.reg_pwd);
        String Name = firstName.getText() + " " + lastName.getText();
        RequestQueue queue = Volley.newRequestQueue(this);
        final String username = String.valueOf(reg_email.getText());
        String url = API_URL + "registration.php?name="+Name+"&email="+username+"&phone="+String.valueOf(reg_phone.getText())+"&password="+String.valueOf(reg_pwd.getText());

        loading.setVisibility(View.VISIBLE);
        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        JSONObject jObject = null;

                        try {
                            jObject = new JSONObject(response);
                            String status = jObject.getString("status");
                            String mssg = jObject.getString("mssg");
                            if(status.equals("Success")){
                                startActivity(new Intent(getApplicationContext(),HomeActivity.class));
                                Toast.makeText(getApplicationContext(), mssg,
                                        Toast.LENGTH_LONG).show();
                                SharedPreferences sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = sharedpreferences.edit();
                                editor.putString("isLoggedIn", "Yes");
                                editor.putString("user_name", username);
                                editor.commit();
                                loading.setVisibility(View.GONE);
                                finishAffinity();
                            }
                            else{
                                Snackbar snackbar = Snackbar
                                        .make(v, mssg, Snackbar.LENGTH_LONG);
                                snackbar.show();
                                loading.setVisibility(View.GONE);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Snackbar snackbar = Snackbar
                                        .make(v, "Could not sign up! Check Connection.", Snackbar.LENGTH_LONG);
                                snackbar.show();
                            loading.setVisibility(View.GONE);
                        }

//                        Log.d("test3",response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                Log.d("test3","\"That didn't work!\"");
                Snackbar snackbar = Snackbar
                        .make(v, "Could not sign up! Server Not Responding.", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        });

        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    public Boolean validate(){
        EditText firstName = (EditText)findViewById(R.id.firstName);
        EditText lastName = (EditText)findViewById(R.id.lastName);
        EditText reg_email = (EditText)findViewById(R.id.reg_email);
        EditText reg_phone = (EditText)findViewById(R.id.reg_phone);
        EditText reg_pwd = (EditText)findViewById(R.id.reg_pwd);

        String PASSWORD_PATTERN = "[a-zA-Z0-9]{2,20}";
        String MobilePattern = "[6-9]{1}+[0-9]{9}";
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

        boolean a = false;
        if (TextUtils.isEmpty(firstName.getText())){
            SnackbarManager.setError(this,"First name cannot be blank!");
            firstName.requestFocus();

        }
        else if (TextUtils.isEmpty(lastName.getText())){
            SnackbarManager.setError(this,"Last name cannot be blank!");
            lastName.requestFocus();

        }
        else if (TextUtils.isEmpty(reg_phone.getText())){
            SnackbarManager.setError(this,"Phone cannot be blank!");
            reg_phone.requestFocus();

        }

        else if (!(reg_phone.getText().toString()).matches(MobilePattern)){
            SnackbarManager.setError(this,"Enter valid Phone");
            reg_phone.requestFocus();

        }
        else if (TextUtils.isEmpty(reg_email.getText())){
            SnackbarManager.setError(this,"Email cannot be blank!");
            reg_email.requestFocus();
        }
        else if (!(reg_email.getText().toString()).matches(emailPattern)){
            SnackbarManager.setError(this,"Enter valid Email");
            reg_email.requestFocus();

        }

        else if (TextUtils.isEmpty(reg_pwd.getText())){
            SnackbarManager.setError(this,"Password cannot be blank!");
            reg_pwd.requestFocus();
        }


        else {
            a = true;
        }
        return a;
    }
}

package com.doctorsonrequest.savatarr.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.doctorsonrequest.savatarr.DoctorListActivity;
import com.doctorsonrequest.savatarr.R;


public class SymptomsAdapter extends RecyclerView.Adapter<SymptomsAdapter.SymptomsViewHolder> {
    private String[] symptoms = {"Cold & Flu","Sore Throat","Fever","Migraine Headache","General Physician","Diarrhea and Vomiting","Stomach Upset/Pain","Gas/Acidity","BP issue (Hypertension/Hypo-tension)","Asthma",
            "Back pain, Neck pain, Joint pain, Knee pain","Infertility"};
    private int[] icon_cat = {R.drawable.common_cold,R.drawable.sour_throat,R.drawable.fever,R.drawable.migraine,R.drawable.general_physician,R.drawable.diarrhea,R.drawable.stomach_ache,R.drawable.acidity,R.drawable.blood_pressure,R.drawable.asthama,R.drawable.back_pain,R.drawable.infertility};
    private Context context;

    public SymptomsAdapter(Context mCtx) {
        this.context = mCtx;
    }


    @NonNull
    @Override
    public SymptomsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_category_layout,parent,false);
        SymptomsViewHolder symptomsViewHolder = new SymptomsViewHolder(view);
        return symptomsViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull SymptomsViewHolder holder, int position) {
//        if(position>=icon_cat.length) {
//            position = 0;
//        }
        String text_id = symptoms[position];
        String finalSpecialty = text_id;
        holder.symptoms.setText(text_id);
        holder.icon_cat.setImageResource(icon_cat[position]);
        holder.ll_item_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, DoctorListActivity.class);
                i.putExtra("specialty", "");
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return symptoms.length;
    }

    public class SymptomsViewHolder extends RecyclerView.ViewHolder {
        public ImageView icon_cat;
        private TextView symptoms;
        private LinearLayout ll_item_home;
        public SymptomsViewHolder(@NonNull View itemView) {
            super(itemView);
            symptoms = itemView.findViewById(R.id.category);
            icon_cat = itemView.findViewById(R.id.icon_cat);
            ll_item_home = itemView.findViewById(R.id.ll_item_home);
        }
    }
}

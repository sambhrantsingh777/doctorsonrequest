package com.doctorsonrequest.savatarr.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.doctorsonrequest.savatarr.PatientDetailActivity;
import com.doctorsonrequest.savatarr.pojoModel.PatientSearchBeanModel;
import com.doctorsonrequest.savatarr.R;

import java.util.List;

public class PatientSearchAdapter extends RecyclerView.Adapter<PatientSearchAdapter.PatientViewHolder> {
    private Context context;
    List<PatientSearchBeanModel.Datum> patientList;


    public PatientSearchAdapter(Context mCtx, List<PatientSearchBeanModel.Datum> patientList) {
        this.context = mCtx;
        this.patientList = patientList;
    }

    @NonNull
    @Override
    public PatientViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_patient_history_layout,parent,false);
        PatientViewHolder patientViewHolder = new PatientViewHolder(view);
        return patientViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull PatientViewHolder holder, int position) {
        final String patient = patientList.get(position).getPatientName();
        final String appointment_time = patientList.get(position).getAppointmentTime();
        final String age = patientList.get(position).getAge();
        final String gender = patientList.get(position).getGender();
        final String status = patientList.get(position).getStatus();
        final String disease = patientList.get(position).getDisease();
        final String severity = patientList.get(position).getSeverity();
        final String symptoms = patientList.get(position).getSymptoms();
        Log.d("testtt", String.valueOf(patientList.get(position)));

        String st = null;
        if (status.equals("1")) {
            st = "Confirm";
        } else if (status.equals("0")) {
            st = "Pending Confirmation";
        } else {
            st = "Pending Confirmation";
        }
        holder.patient_name.setText(patient);
        holder.patient_age.setText("Age: "+age);
        holder.date_time.setText(appointment_time);
        holder.status.setText(st);
        holder.ll_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, PatientDetailActivity.class);
                intent.putExtra("patient", patient);
                intent.putExtra("age", age);
                intent.putExtra("disease", disease);
                intent.putExtra("status", status);
                intent.putExtra("appointment_time", appointment_time);
                intent.putExtra("severity", severity);
                intent.putExtra("symptoms", symptoms);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return patientList.size();
    }

    public class PatientViewHolder extends RecyclerView.ViewHolder {
        public TextView patient_name;
        public TextView date_time;
        public TextView patient_age;
        public TextView status;
        public ImageView gender_image;
        private LinearLayout ll_item;
        public PatientViewHolder(@NonNull View itemView) {
            super(itemView);
            ll_item = itemView.findViewById(R.id.ll_item);
            patient_name = itemView.findViewById(R.id.patient_name);
            patient_age = itemView.findViewById(R.id.patient_age);
            date_time = itemView.findViewById(R.id.date_time);
            status = itemView.findViewById(R.id.status);
        }
    }
}

package com.doctorsonrequest.savatarr.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.doctorsonrequest.savatarr.DoctorDetailActivity;
import com.doctorsonrequest.savatarr.pojoModel.SearchBeanModel;
import com.doctorsonrequest.savatarr.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.SearchViewHolder> {
    private Context context;
    List<SearchBeanModel.Datum> searchList;

    public SearchAdapter(Context mCtx, List<SearchBeanModel.Datum> response) {
        this.context = mCtx;
        this.searchList = response;
    }

    @NonNull
    @Override
    public SearchViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_directory_layout,parent,false);
        SearchViewHolder searchViewHolder = new SearchViewHolder(view);
        return searchViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull SearchViewHolder holder, int position) {
        final String name = searchList.get(position).getName();
        final String sex = searchList.get(position).getGender();
        final String category = searchList.get(position).getSpecialty();
        final String desc = searchList.get(position).getDescription();
        final String img = searchList.get(position).getProfilePic();
        holder.dr_name.setText(name);
        final String gender;
        if(sex.equals("1")){
            gender = "Male";
        }
        else{
            gender = "Female";
        }
        holder.gender.setText(gender);
        holder.speciality.setText(category);
        holder.details.setText(desc);

        Picasso.get().load("https://justtripit.herokuapp.com/api/images/"+img).into(holder.profile_image);

        holder.ll_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, DoctorDetailActivity.class);
                intent.putExtra("name", name);
                intent.putExtra("gender", gender);
                intent.putExtra("specialty", category);
                intent.putExtra("desc", desc);
                intent.putExtra("prof_pic", img);
                context.startActivity(intent);
            }
        });


    }

    @Override
    public int getItemCount() {
        return searchList.size();
    }

    public class SearchViewHolder extends RecyclerView.ViewHolder {
        public TextView dr_name;
        public TextView gender;
        public TextView speciality;
        public TextView details;
        public ImageView profile_image;
        private LinearLayout ll_item;
        public SearchViewHolder(@NonNull View itemView) {
            super(itemView);
            ll_item = itemView.findViewById(R.id.ll_item);
            dr_name = itemView.findViewById(R.id.dr_name);
            gender = itemView.findViewById(R.id.gender);
            speciality = itemView.findViewById(R.id.speciality);
            details = itemView.findViewById(R.id.details);
            profile_image = itemView.findViewById(R.id.profile_image);
        }
    }
}

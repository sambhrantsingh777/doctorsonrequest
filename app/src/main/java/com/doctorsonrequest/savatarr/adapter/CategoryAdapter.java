package com.doctorsonrequest.savatarr.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.doctorsonrequest.savatarr.DoctorListActivity;
import com.doctorsonrequest.savatarr.R;

import java.util.Arrays;


public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder> {
    private String[] category = {"General physician","Orthopedist","Pediatrician/Child Specialist","General Surgeon ","Ophthalmologist","Gynecologist-Obstetrician","Dentist","Physiotherapist","Nursing Care at home"};
    private int[] icon_cat = {R.drawable.physician, R.drawable.orthopedic,R.drawable.pediatric,R.drawable.surgeon,R.drawable.opthalmologist,R.drawable.gynecologist,R.drawable.dentist,R.drawable.physiotherapy,R.drawable.nursing_at_home};
    private Context context;

    public CategoryAdapter(Context mCtx) {
        this.context = mCtx;
    }


    @NonNull
    @Override
    public CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_category_layout,parent,false);
        CategoryViewHolder categoryViewHolder = new CategoryViewHolder(view);
        return categoryViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryViewHolder holder, int position) {
        String text_id = category[position];
        String specialty = text_id;
        if(text_id.equals("Pediatrician/Child Specialist")){
            specialty = "Pediatric";
        }
        else if( Arrays.asList("Gynecologist-Obstetrician","Dentist","Physiotherapist","Nursing Care at home").contains(text_id) ){
            specialty = "";
        }
        holder.category.setText(text_id);
        holder.icon_cat.setImageResource(icon_cat[position]);
        final String finalSpecialty = specialty;
        holder.ll_item_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, DoctorListActivity.class);
                i.putExtra("specialty", finalSpecialty);
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return category.length;
    }

    public class CategoryViewHolder extends RecyclerView.ViewHolder {
        public ImageView icon_cat;
        private TextView category;
        private LinearLayout ll_item_home;
        public CategoryViewHolder(@NonNull View itemView) {
            super(itemView);
            category = itemView.findViewById(R.id.category);
            icon_cat = itemView.findViewById(R.id.icon_cat);
            ll_item_home = itemView.findViewById(R.id.ll_item_home);
        }
    }
}

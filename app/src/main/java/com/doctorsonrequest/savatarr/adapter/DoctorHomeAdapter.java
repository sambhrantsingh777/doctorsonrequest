package com.doctorsonrequest.savatarr.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.doctorsonrequest.savatarr.PatientDetailActivity;
import com.doctorsonrequest.savatarr.R;

import java.util.ArrayList;

public class DoctorHomeAdapter extends RecyclerView.Adapter<DoctorHomeAdapter.DoctorViewHolder> {
    private final ArrayList<String> patient_name;
    private final ArrayList<String> apointment_time;
    private final ArrayList<String> ages;
    private final ArrayList<String> genders;
    private final ArrayList<String> statusLists;
    private final ArrayList<String> diseaseLists;
    private Context context;

    public DoctorHomeAdapter(Context mCtx, ArrayList<String> patient_name, ArrayList<String> appointment_time, ArrayList<String> age, ArrayList<String> gender, ArrayList<String> statusList,ArrayList<String> diseaseList) {
        this.context = mCtx;
        this.patient_name = patient_name;
        this.apointment_time = appointment_time;
        this.ages = age;
        this.genders = gender;
        this.statusLists = statusList;
        this.diseaseLists = diseaseList;
    }

    @NonNull
    @Override
    public DoctorViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_patient_history_layout,parent,false);
        DoctorViewHolder doctorViewHolder = new DoctorViewHolder(view);
        return doctorViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull DoctorViewHolder holder, int position) {
        final String patient = patient_name.get(position);
        final String appointment_time = apointment_time.get(position);
        final String age = ages.get(position);
        final String gender = genders.get(position);
        final String status = statusLists.get(position);
        final String disease = diseaseLists.get(position);
        String st = null;
        if (status.equals("1")) {
            st = "Confirm";
        } else if (status.equals("0")) {
            st = "Pending Confirmation";
        } else {
            st = "Pending Confirmation";
        }
        holder.patient_name.setText(patient);
        holder.patient_age.setText("Age: "+age);
        holder.date_time.setText(appointment_time);
        holder.status.setText(st);
        holder.ll_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, PatientDetailActivity.class);
                intent.putExtra("patient", patient);
                intent.putExtra("age", age);
                intent.putExtra("disease", disease);
                intent.putExtra("status", status);
                intent.putExtra("appointment_time", appointment_time);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return patient_name.size();
    }

    public class DoctorViewHolder extends RecyclerView.ViewHolder {
        public TextView patient_name;
        public TextView date_time;
        public TextView patient_age;
        public TextView status;
        public ImageView gender_image;
        private LinearLayout ll_item;
        public DoctorViewHolder(@NonNull View itemView) {
            super(itemView);
            ll_item = itemView.findViewById(R.id.ll_item);
            patient_name = itemView.findViewById(R.id.patient_name);
            patient_age = itemView.findViewById(R.id.patient_age);
            date_time = itemView.findViewById(R.id.date_time);
            status = itemView.findViewById(R.id.status);
        }
    }
}

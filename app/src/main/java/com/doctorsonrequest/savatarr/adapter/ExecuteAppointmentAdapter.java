package com.doctorsonrequest.savatarr.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.doctorsonrequest.savatarr.R;
import com.squareup.picasso.Picasso;


import java.util.ArrayList;

import static com.doctorsonrequest.savatarr.SplashActivity.API_URL;


public class ExecuteAppointmentAdapter extends RecyclerView.Adapter<ExecuteAppointmentAdapter.AppointmentViewHolder> {
    private final ArrayList<String> doctor_assigned;
    private final ArrayList<String> appointment_time;
    private final ArrayList<String> statusList;
    private final ArrayList<String> profile_pic;
    private Context mCtx;

    public ExecuteAppointmentAdapter(Context mCtx, ArrayList<String> doctor_assigned, ArrayList<String> appointment_time, ArrayList<String> statusList, ArrayList<String> profile_pic) {
        this.mCtx=mCtx;
        this.doctor_assigned = doctor_assigned;
        this.appointment_time = appointment_time;
        this.statusList = statusList;
        this.profile_pic = profile_pic;
    }

    @NonNull
    @Override
    public AppointmentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_execute_appointment_layout,parent,false);
        AppointmentViewHolder appointmentViewHolder = new AppointmentViewHolder(view);
        return appointmentViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull AppointmentViewHolder holder, int position) {
        final String doc = doctor_assigned.get(position);
        final String time = appointment_time.get(position);
        final String status = statusList.get(position);
        String st = null;
        if (status.equals("1")) {
            st = "Confirm";
        } else if (status.equals("0")) {
            st = "Pending Confirmation";
        } else {
            st = "Pending Confirmation";
        }
        holder.dr_name.setText(doc);
        holder.specialty.setText(doc);
        holder.dateTime.setText(time);
        holder.status.setText(st);
        final String img = profile_pic.get(position);

        Picasso.get().load(API_URL+"images/"+img).into(holder.profile_image);
    }

    @Override
    public int getItemCount() {
//        Log.d("test", String.valueOf(doctor_assigned.size()));
        return doctor_assigned.size();
    }

    public class AppointmentViewHolder extends RecyclerView.ViewHolder {
        public TextView dr_name;
        public TextView specialty;
        public TextView dateTime;
        public TextView status;
        public ImageView profile_image;
        private LinearLayout ll_item;
        public AppointmentViewHolder(@NonNull View itemView) {
            super(itemView);
            ll_item = itemView.findViewById(R.id.ll_item);
            dr_name = itemView.findViewById(R.id.dr_name);
            specialty = itemView.findViewById(R.id.speciality);
            dateTime = itemView.findViewById(R.id.date);
            status = itemView.findViewById(R.id.status);
            profile_image = itemView.findViewById(R.id.profile_image);
        }
    }
}

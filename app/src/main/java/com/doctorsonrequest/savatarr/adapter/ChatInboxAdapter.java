package com.doctorsonrequest.savatarr.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.doctorsonrequest.savatarr.ChatActivity;
import com.doctorsonrequest.savatarr.R;


public class ChatInboxAdapter extends RecyclerView.Adapter<ChatInboxAdapter.InboxViewHolder> {
    private Context context;


    public ChatInboxAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public InboxViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_chat_indox_layout,parent,false);
        InboxViewHolder inboxViewHolder = new InboxViewHolder(view);
        return inboxViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull InboxViewHolder holder, int position) {
        holder.ll_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, ChatActivity.class));
            }
        });

    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public class InboxViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout ll_chat;

        public InboxViewHolder(@NonNull View itemView) {
            super(itemView);
            ll_chat = itemView.findViewById(R.id.ll_chat);
        }
    }
}

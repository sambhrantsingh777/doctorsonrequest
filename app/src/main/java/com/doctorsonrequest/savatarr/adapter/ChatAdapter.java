package com.doctorsonrequest.savatarr.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.doctorsonrequest.savatarr.R;

import java.util.List;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ChatImageViewHolder> {
    private static final int RCV_DATA_TYPE = 0;
    private static final int SENT_DATA_TYPE = 1;
    int[]  a = { 1,2,3,4,5,6,7 };
    private String[] category = {};
    private Context context;

    public ChatAdapter(Context context) {
        this.context = context;

    }

    @NonNull
    @Override
    public ChatImageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

//        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_inbox_layout, parent, false);
//        ChatImageViewHolder cvh = new ChatImageViewHolder(view);
//        return cvh;
        if (viewType == RCV_DATA_TYPE) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_chat_rcv, parent, false);
            ChatImageViewHolder cvh = new ChatImageViewHolder(view);

            return cvh;
        } else if (viewType == SENT_DATA_TYPE) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_chat_sent, parent, false);
            ChatImageViewHolder cvh = new ChatImageViewHolder(view);

            return cvh;
        }
        else {
            return null;
        }

    }

    @Override
    public void onBindViewHolder(@NonNull ChatImageViewHolder holder, int position) {
        String text_id = category[position];
        holder.message_text_view.setText(text_id);

    }

    @Override
    public int getItemViewType(int position) {

        if (a.length%2==0){
            return SENT_DATA_TYPE;
        }
        else {
            return SENT_DATA_TYPE;
        }
    }

    @Override
    public int getItemCount() {
        return category.length;
    }

    public class ChatImageViewHolder extends RecyclerView.ViewHolder {
        private TextView message_text_view;
        public ChatImageViewHolder(@NonNull View itemView) {
            super(itemView);
            message_text_view = itemView.findViewById(R.id.message_text_view);
        }
    }
}


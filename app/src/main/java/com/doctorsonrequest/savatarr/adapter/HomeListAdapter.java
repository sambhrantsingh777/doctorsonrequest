package com.doctorsonrequest.savatarr.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.doctorsonrequest.savatarr.DoctorDetailActivity;
import com.doctorsonrequest.savatarr.dataHandler;
import com.doctorsonrequest.savatarr.R;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

import static com.doctorsonrequest.savatarr.SplashActivity.API_URL;


public class HomeListAdapter extends RecyclerView.Adapter<HomeListAdapter.HomeViewHolder> {
    private Context context;
    final ArrayList<String> names = new ArrayList<String>();
    final ArrayList<String> gender = new ArrayList<String>();
    final ArrayList<String> speciality = new ArrayList<String>();
    final ArrayList<String> profile_pic = new ArrayList<String>();
    final ArrayList<String> description = new ArrayList<String>();

    public HomeListAdapter(Context context, dataHandler dh) {
        this.context = context;

        Map<String, Map<String, String>> data = dh.get();
        for (Map.Entry<String, Map<String, String>> entry : data.entrySet()){
            Map<String, String> mp = new TreeMap<String, String>(entry.getValue());
            int count = 0;
            for(Map.Entry<String, String> entr:mp.entrySet()){
                if(count==0){
                    description.add(entr.getValue());
                }
                else if(count==1){
                    gender.add(entr.getValue());
                }
                else if(count==2){
                    names.add(entr.getValue());
                }
                else if(count==3){
                    profile_pic.add(entr.getValue());
                }
                else if(count==4){
                    speciality.add(entr.getValue());
                }
                else{
                    count=-1;
                }
                count++;
            }
        }
    }
    @NonNull
    @Override
    public HomeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_directory_layout,parent,false);
        HomeViewHolder homeViewHolder = new HomeViewHolder(view);

        return homeViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull HomeViewHolder holder, int position) {
        final String name = names.get(position);
        final String sex = gender.get(position);
        final String category = speciality.get(position);
        final String desc = description.get(position);
        final String img = profile_pic.get(position);
        holder.dr_name.setText(name);
        holder.gender.setText(sex);
        holder.speciality.setText(category);
        holder.details.setText(desc);

        Picasso.get().load(API_URL+"images/"+img).into(holder.profile_image);

        holder.ll_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, DoctorDetailActivity.class);
                intent.putExtra("name", name);
                intent.putExtra("gender", sex);
                intent.putExtra("specialty", category);
                intent.putExtra("desc", desc);
                intent.putExtra("prof_pic", img);
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return names.size();
    }

    public class HomeViewHolder extends RecyclerView.ViewHolder {
        public TextView dr_name;
        public TextView gender;
        public TextView speciality;
        public TextView details;
        public ImageView profile_image;
        private LinearLayout ll_item;
        public HomeViewHolder(@NonNull View itemView) {
            super(itemView);
            ll_item = itemView.findViewById(R.id.ll_item);
            dr_name = itemView.findViewById(R.id.dr_name);
            gender = itemView.findViewById(R.id.gender);
            speciality = itemView.findViewById(R.id.speciality);
            details = itemView.findViewById(R.id.details);
            profile_image = itemView.findViewById(R.id.profile_image);
        }
    }
}

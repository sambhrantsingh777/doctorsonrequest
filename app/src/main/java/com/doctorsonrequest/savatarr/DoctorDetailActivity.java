package com.doctorsonrequest.savatarr;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.doctorsonrequest.savatarr.R;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import static com.doctorsonrequest.savatarr.SplashActivity.API_URL;
import static com.doctorsonrequest.savatarr.SplashActivity.MyPREFERENCES;

public class DoctorDetailActivity extends AppCompatActivity implements PaymentResultListener {
    private static final int ONE_MINUTE_IN_MILLIS = 60000;
    private Button btn_consult_Now;
    public Context mCtx;
    public String dr_name;
    private int ActivityRequestCode=200;
    private String date;
    private String timeslot;
    private String appointment_id;
    private View mainView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_detail);
        Checkout.preload(getApplicationContext());
        mCtx = this;
        btn_consult_Now = findViewById(R.id.btn_consult_Now);
        btn_consult_Now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConsultNow();
            }
        });

        dr_name = getIntent().getStringExtra("name");
        String gender = getIntent().getStringExtra("gender");
        String specialty = getIntent().getStringExtra("specialty");
        String desc = getIntent().getStringExtra("desc");
        String prof_pic = getIntent().getStringExtra("prof_pic");
        TextView d_name = (TextView)findViewById(R.id.d_name);
        TextView d_gender = (TextView)findViewById(R.id.d_gender);
        TextView d_specialty = (TextView)findViewById(R.id.d_specialty);
        TextView d_desc = (TextView)findViewById(R.id.d_desc);
        d_name.setText(dr_name);
        d_gender.setText(gender);
        d_specialty.setText(specialty);
        d_desc.setText(desc);

        ImageView d_prof_pic = (ImageView)findViewById(R.id.d_prof_pic);
        Picasso.get().load(API_URL+"images/"+prof_pic).into(d_prof_pic);

        ImageView goBack = (ImageView)findViewById(R.id.go_back);
        goBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
    private void ConsultNow(){

        final AlertDialog.Builder update = new AlertDialog.Builder(mCtx);
//        View mView = getLayoutInflater().inflate(R.layout.dialog_consult_now_layout, null);

        LayoutInflater layoutInflater = LayoutInflater.from(mCtx);

        final View mView = layoutInflater.inflate(R.layout.dialog_consult_now_layout, null);

        List<String> times = new ArrayList<String>();
        Calendar date = Calendar.getInstance();
        long t = date.getTimeInMillis();
        int index = 0;
        while (index<72){
            t += (10 * ONE_MINUTE_IN_MILLIS);
            Date afterAddingTenMins=new Date(t);
            String currentTime = new SimpleDateFormat("HH:mm", Locale.getDefault()).format(afterAddingTenMins);
            times.add(currentTime);
            index++;
        }

        String[] timeSlot = new String[times.size()];
        times.toArray(timeSlot);
        String[] gender = {"Gender","Male","Female"};
        String[] severities = {"Severity","Mild","Moderate","Severe"};
        final Spinner sp_gender = (Spinner) mView.findViewById(R.id.sp_gender);
        final Spinner sp_time_slot = (Spinner) mView.findViewById(R.id.sp_time_slot);
        final Spinner sp_severity = (Spinner) mView.findViewById(R.id.ed_sevierity);
        final TextInputEditText ed_date = (TextInputEditText) mView.findViewById(R.id.ed_date);


        Button btn_book_appointment = (Button) mView.findViewById(R.id.btn_book_appointment);
        update.setView(mView);
        final AlertDialog alertDialog = update.create();
        alertDialog.setCanceledOnTouchOutside(false);


        ArrayAdapter timeArray= new ArrayAdapter(this,android.R.layout.simple_spinner_item,timeSlot);
        timeArray.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_time_slot.setAdapter(timeArray);

        ArrayAdapter genderArray= new ArrayAdapter(this,android.R.layout.simple_spinner_item,gender);
        genderArray.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_gender.setAdapter(genderArray);

        ArrayAdapter severityArray= new ArrayAdapter(this,android.R.layout.simple_spinner_item,severities);
        severityArray.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_severity.setAdapter(severityArray);

        alertDialog.show();
        btn_book_appointment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainView = v;
                submitRequest(mView);

            }
        });
        ed_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                final int mYear, mMonth, mDay, mHour, mMinute;
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);
                c.add(Calendar.YEAR, 0);

                DatePickerDialog datePickerDialog = new DatePickerDialog(mCtx,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                ed_date.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" +year);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());
                datePickerDialog.show();
            }
        });
    }

    public boolean submitRequest(View v){
        EditText ed_name = v.findViewById(R.id.ed_name);
        Spinner sp_gender = v.findViewById(R.id.sp_gender);
        EditText ed_age = v.findViewById(R.id.ed_age);
        EditText ed_disease = v.findViewById(R.id.ed_disease);
        Spinner ed_sevierity = v.findViewById(R.id.ed_sevierity);
        EditText ed_symptoms = v.findViewById(R.id.ed_symptoms);
        EditText ed_date = v.findViewById(R.id.ed_date);
        Spinner sp_time_slot = v.findViewById(R.id.sp_time_slot);
        String error = "";
        if(String.valueOf(ed_name.getText()).equals("")){
            error = "Name cannot be blank!";
        }
        if(String.valueOf(sp_gender.getSelectedItem()).equals("")||String.valueOf(sp_gender.getSelectedItem()).equals("Gender")){
            error = "Gender cannot be blank!";
        }
        if(String.valueOf(ed_age.getText()).equals("")){
            error = "Age cannot be blank!";
        }
        if(ed_sevierity.getSelectedItem().toString().equals("")||ed_sevierity.getSelectedItem().toString().equals("Severity")){
            error = "Severity cannot be blank!";
        }
        if(String.valueOf(ed_symptoms.getText()).equals("")){
            error = "Symptoms cannot be blank!";
        }
        if(String.valueOf(ed_date.getText()).equals("")){
            error = "Date cannot be blank!";
        }
        if(sp_time_slot.getSelectedItem().toString().equals("")){
            error = "Time Slot cannot be blank!";
        }
        if(!error.equals("")){
            Snackbar snackbar = Snackbar
                    .make(v, error, Snackbar.LENGTH_LONG);
            snackbar.show();
            return false;
        }
        else {
            date = String.valueOf(ed_date.getText());
            timeslot = sp_time_slot.getSelectedItem().toString();
            bookAppointmentRequest(v, String.valueOf(ed_name.getText()), sp_gender.getSelectedItem().toString(), String.valueOf(ed_age.getText()), String.valueOf(ed_disease.getText()), ed_sevierity.getSelectedItem().toString(), String.valueOf(ed_symptoms.getText()), timeslot, date);
            return true;
        }
    }

    public void bookAppointmentRequest(final View v, final String name, final String gender, final String age, final String diseas, final String severity, final String symptoms, final String time_slot, final String date) {
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        String url = API_URL + "appointment.php";
        // Request a string response from the provided URL.
        StringRequest sr = new StringRequest(Request.Method.POST,url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                JSONObject jObject;
                try {
                    jObject = new JSONObject(response);
                    String status = jObject.getString("status");
                    String mssg = jObject.getString("mssg");
                    String email = jObject.getString("email");
                    String mobile = jObject.getString("mobile");
                    appointment_id = jObject.getString("apt_id");
                    if(status.equals("Success")){
                        startPayment(email, mobile);
                    }
                    else{
                        Snackbar snackbar = Snackbar
                                .make(v, mssg, Snackbar.LENGTH_LONG);
                        snackbar.show();
                    }
                }
                catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("savatarr_errorOnCall", String.valueOf(error));
                Snackbar snackbar = Snackbar
                        .make(v, "Some error occurred while updating profile. Check your connection.", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                SharedPreferences sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                String user_name = sharedpreferences.getString("user_name", "NULL");
                params.put("user_name", user_name);
                params.put("name", name);
                params.put("age", age);
                int g;
                if(gender.equals("Male")){
                    g = 1;
                }
                else{
                    g=0;
                }
                params.put("gender", String.valueOf(g));
                params.put("disease", diseas);
                params.put("severity", severity);
                params.put("symptoms", symptoms);
                params.put("doctor", dr_name);
                params.put("slot", time_slot);
                params.put("date", date);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/x-www-form-urlencoded");
                return params;
            }
        };
        queue.add(sr);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ActivityRequestCode && data != null) {
            Toast.makeText(this, data.getStringExtra("nativeSdkForMerchantMessage") + data.getStringExtra("response"), Toast.LENGTH_SHORT).show();
        }
    }

    public void startPayment(String email, String mobile) {

        final Activity activity = this;

        final Checkout co = new Checkout();
        co.setKeyID(Constants.RAZOR_KEY);
        try {
            JSONObject options = new JSONObject();
            options.put("name", "Doctors on Request");
            options.put("description", "Appointment Fees");
            //You can omit the image option to fetch the image from dashboard
            options.put("image", Constants.LOGO_URL);
            options.put("currency", "INR");
            options.put("amount", "14900");

            JSONObject preFill = new JSONObject();
            preFill.put("email", email);
            preFill.put("contact", mobile);

            options.put("prefill", preFill);

            co.open(activity, options);
        } catch (Exception e) {
            Toast.makeText(activity, "Error in payment: " + e.getMessage(), Toast.LENGTH_SHORT)
                    .show();
            e.printStackTrace();
        }
    }

    @Override
    public void onPaymentSuccess(String s) {
        try {
            View v = mainView;
            confirmPayment(appointment_id, v);
            Intent intent = new Intent(mCtx, AppointmentConfirmActivity.class);
            intent.putExtra("drName",dr_name.trim());
            intent.putExtra("date",date+' '+timeslot);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);

        } catch (Exception e) {
            Log.e("RAZORTEST", "Exception in onPaymentSuccess", e);
        }
    }

    @Override
    public void onPaymentError(int i, String s) {
        try {
            Toast.makeText(this, "Payment failed: " + i + " " + s, Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Log.e("RAZORTEST", "Exception in onPaymentError", e);
        }
    }

    public void confirmPayment(String apt_id, View v) {
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        String url = API_URL + "validate.php";
        // Request a string response from the provided URL.
        StringRequest sr = new StringRequest(Request.Method.POST,url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                JSONObject jObject;
                try {
                    jObject = new JSONObject(response);
                    String status = jObject.getString("status");
                    Log.d("TESTME",status);
                    String mssg;
                    if(status.equals("200")){
                        mssg= "Payment Confirmed.";
                    }
                    else{
                        mssg= "Payment Validation Failed.";
                    }
                    Snackbar snackbar = Snackbar
                            .make(v, mssg, Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
                catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("savatarr_errorOnCall", String.valueOf(error));
                Snackbar snackbar = Snackbar
                        .make(v, "Some error occurred while confirming payment. Check your connection. Or contact us within 24 hrs.", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("apt_id", apt_id);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/x-www-form-urlencoded");
                return params;
            }
        };
        queue.add(sr);
    }
}

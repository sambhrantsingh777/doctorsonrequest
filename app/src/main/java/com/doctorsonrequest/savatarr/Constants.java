package com.doctorsonrequest.savatarr;

import android.content.Context;
import android.content.SharedPreferences;

import com.doctorsonrequest.savatarr.utils.PreferenceManager;

import static com.doctorsonrequest.savatarr.SplashActivity.API_URL;

public class Constants {

    public static final String BASE_URL = "https://justtripit.herokuapp.com/api/";
    public static String RAZOR_KEY = "rzp_test_lPBSoQJyPPDoCw";
    public static String LOGO_URL = API_URL+"images/logo.png";
    public static String COMAT_APP_ID = "201846ba4cf301c";
    public static String COMAT_AUTH_KEY = "a69c0a4b05fbcb9c50e25ea08062bc27ddfba4e9";
    public static String COMAT_REGION = "us";
}

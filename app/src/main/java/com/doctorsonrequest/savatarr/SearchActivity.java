package com.doctorsonrequest.savatarr;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;

import com.doctorsonrequest.savatarr.adapter.PatientSearchAdapter;
import com.doctorsonrequest.savatarr.adapter.SearchAdapter;
import com.doctorsonrequest.savatarr.pojoModel.PatientSearchBeanModel;
import com.doctorsonrequest.savatarr.pojoModel.SearchBeanModel;
import com.doctorsonrequest.savatarr.utils.ResponseHandler;
import com.doctorsonrequest.savatarr.utils.ServiceWrapper;
import com.doctorsonrequest.savatarr.utils.SnackbarManager;
import com.google.gson.JsonObject;

import java.util.List;

import retrofit2.Call;

import static com.doctorsonrequest.savatarr.SplashActivity.MyPREFERENCES;

public class SearchActivity extends AppCompatActivity {
    private AutoCompleteTextView et_search;
    InputMethodManager imm;
    private ImageView iv_back;
    String searchStr;
    private RecyclerView recycle_search;
    private RecyclerView.LayoutManager layoutManager;
    private Context mCtx;
    private List<SearchBeanModel.Datum> searchList;
    private List<PatientSearchBeanModel.Datum> patientList;
    private static final String [] search= new String[]{"General physician","Orthopedist","Pediatrician/Child Specialist","General Surgeon ","Ophthalmologist","Gynecologist-Obstetrician","Dentist","Physiotherapist","Nursing Care at home",
            "Cold & Flu","Sore Throat","Fever","Migraine Headache","General Physician","Diarrhea and Vomiting","Stomach Upset/Pain","Gas/Acidity","BP issue (Hypertension/Hypo-tension)","Asthma",
            "Back pain, Neck pain, Joint pain, Knee pain","Infertility"};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        mCtx = this;
        SharedPreferences sharedpreferences = mCtx.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        final String doc_name = sharedpreferences.getString("doc_name", "NULL");
        recycle_search = findViewById(R.id.recycle_search);
        layoutManager = new LinearLayoutManager(mCtx,LinearLayoutManager.VERTICAL,false);
        recycle_search.setLayoutManager(layoutManager);




        et_search = findViewById(R.id.et_search);
        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        iv_back = findViewById(R.id.iv_back);
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                onBackPressed();
            }
        });

        et_search.requestFocus();

        if(!doc_name.equals("NULL")){

        }else {
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item,search);
            et_search.setAdapter(adapter);
        }



        et_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

//                SnackbarManager.successResponse(mCtx,txt_search.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
                searchStr = et_search.getText().toString();
                if(!doc_name.equals("NULL")){
                    Log.d("TESTME", doc_name);
                    SearchPatient(doc_name);
                }else {
                    SearchDoctor();
                }
            }
        });
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
    private void SearchDoctor(){
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("search",searchStr);
        ServiceWrapper serviceWrapper = new ServiceWrapper(mCtx);
        Call<SearchBeanModel> call = serviceWrapper.getService().getSearch(searchStr);
        serviceWrapper.ReqHandlerDoctor(call,  new ResponseHandler<SearchBeanModel>(mCtx) {
            @Override
            public void onStarted() {
                Log.d("TESTME", String.valueOf(searchList));
            }

            @Override
            public void onResponse(SearchBeanModel response) {
                if(response.getStatus().equals("Success")) {
                    searchList = response.getData();
                    Log.d("TESTME", String.valueOf(searchList));
                    SearchAdapter searchAdapter = new SearchAdapter(mCtx,searchList);
                    recycle_search.setAdapter(searchAdapter);
                }else {
                    SnackbarManager.sException(mCtx,"No Results!");
                }

            }
        });
    }

//    private void SearchPatient(String doc_name) {
//        Retrofit retrofit = new Retrofit.Builder()
//                .baseUrl(Constants.BASE_URL)
//                .addConverterFactory(GsonConverterFactory.create())
//                .build();
//        JsonObject jsonObject = new JsonObject();
//        jsonObject.addProperty("doc_name",doc_name);
//        jsonObject.addProperty("keyword",searchStr);
//        ServiceInterface service = retrofit.create(ServiceInterface.class);
//        Call<SearchBeanModel> call = service.getSearch(searchStr);
//        call.enqueue(new Callback<SearchBeanModel>() {
//            @Override
//            public void onResponse(Call<SearchBeanModel> call, Response<SearchBeanModel> response) {
//                if(response.isSuccessful()){
//                    searchList = response.body().getData();
//                    SearchAdapter searchAdapter = new SearchAdapter(mCtx,searchList);
//                    recycle_search.setAdapter(searchAdapter);
//                }
//            }
//
//            @Override
//            public void onFailure(Call<SearchBeanModel> call, Throwable t) {
//
//            }
//        });
//
//    }

    private void SearchPatient(String doc_name){
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("doc_name",doc_name);
        jsonObject.addProperty("keyword",searchStr);
        ServiceWrapper serviceWrapper = new ServiceWrapper(mCtx);
        Call<PatientSearchBeanModel> call = serviceWrapper.getService().getSearchPatient(doc_name,searchStr);
        serviceWrapper.ReqHandlerDoctor(call,  new ResponseHandler<PatientSearchBeanModel>(mCtx) {
            @Override
            public void onStarted() {

            }

            @Override
            public void onResponse(PatientSearchBeanModel response) {
                if(response.getStatus().equals("Success")) {
                    patientList = response.getData();
                    PatientSearchAdapter patientSearchAdapter = new PatientSearchAdapter(mCtx,patientList);
                    recycle_search.setAdapter(patientSearchAdapter);
                }else {
                    SnackbarManager.sException(mCtx,"No Results!");
                }
            }
        });
    }
}

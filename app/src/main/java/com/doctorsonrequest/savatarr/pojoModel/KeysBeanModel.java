package com.doctorsonrequest.savatarr.pojoModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class KeysBeanModel {
    @SerializedName("status_code")
    @Expose
    private String statusCode;
    @SerializedName("mssg")
    @Expose
    private String mssg;
    @SerializedName("data")
    @Expose
    private Data data;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getMssg() {
        return mssg;
    }

    public void setMssg(String mssg) {
        this.mssg = mssg;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
    public class Data {

        @SerializedName("razor_key")
        @Expose
        private String razorKey;
        @SerializedName("comat_app_id")
        @Expose
        private String comatAppId;
        @SerializedName("comat_auth_key")
        @Expose
        private String comatAuthKey;
        @SerializedName("comat_region")
        @Expose
        private String comatRegion;

        public String getRazorKey() {
            return razorKey;
        }

        public void setRazorKey(String razorKey) {
            this.razorKey = razorKey;
        }

        public String getComatAppId() {
            return comatAppId;
        }

        public void setComatAppId(String comatAppId) {
            this.comatAppId = comatAppId;
        }

        public String getComatAuthKey() {
            return comatAuthKey;
        }

        public void setComatAuthKey(String comatAuthKey) {
            this.comatAuthKey = comatAuthKey;
        }

        public String getComatRegion() {
            return comatRegion;
        }

        public void setComatRegion(String comatRegion) {
            this.comatRegion = comatRegion;
        }

    }
}

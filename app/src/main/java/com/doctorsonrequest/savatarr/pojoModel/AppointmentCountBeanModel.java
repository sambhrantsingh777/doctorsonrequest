package com.doctorsonrequest.savatarr.pojoModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AppointmentCountBeanModel {
    @SerializedName("status_code")
    @Expose
    private String statusCode;
    @SerializedName("mssg")
    @Expose
    private String mssg;
    @SerializedName("data")
    @Expose
    private Data data;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getMssg() {
        return mssg;
    }

    public void setMssg(String mssg) {
        this.mssg = mssg;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
    public class Data {

        @SerializedName("month")
        @Expose
        private String month;
        @SerializedName("week")
        @Expose
        private String week;
        @SerializedName("total")
        @Expose
        private String total;

        public String getMonthly() {
            return month;
        }

        public void setMonthly(String month) {
            this.month = month;
        }

        public String getWeekly() {
            return week;
        }

        public void setWeekly(String week) {
            this.week = week;
        }

        public String getTotal() {
            return total;
        }

        public void setTotal(String total) {
            this.total = total;
        }

    }
}


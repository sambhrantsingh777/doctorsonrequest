package com.doctorsonrequest.savatarr.pojoModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class PatientSearchBeanModel {
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("data")
    @Expose
    private List<Datum> data = new ArrayList<Datum>();

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }
    public class Datum {

        @SerializedName("disease")
        @Expose
        private String disease;
        @SerializedName("date")
        @Expose
        private String date;
        @SerializedName("patient_name")
        @Expose
        private String patientName;
        @SerializedName("appointment_time")
        @Expose
        private String appointmentTime;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("gender")
        @Expose
        private String gender;
        @SerializedName("age")
        @Expose
        private String age;
        @SerializedName("data")
        @Expose
        private String data;
        @SerializedName("severity")
        @Expose
        private String severity;
        @SerializedName("symptoms")
        @Expose
        private String symptoms;

        public String getDisease() {
            return disease;
        }

        public void setDisease(String disease) {
            this.disease = disease;
        }

        public void setSeverity(String severity) {
            this.severity = severity;
        }

        public String getSeverity() {
            return severity;
        }

        public void setSymptoms(String symptoms) {
            this.symptoms = symptoms;
        }

        public String getSymptoms() {
            return symptoms;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getPatientName() {
            return patientName;
        }

        public void setPatientName(String patientName) {
            this.patientName = patientName;
        }

        public String getAppointmentTime() {
            return appointmentTime;
        }

        public void setAppointmentTime(String appointmentTime) {
            this.appointmentTime = appointmentTime;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getAge() {
            return age;
        }

        public void setAge(String age) {
            this.age = age;
        }

        public String getData() {
            return data;
        }

        public void setData(String data) {
            this.data = data;
        }

    }
}

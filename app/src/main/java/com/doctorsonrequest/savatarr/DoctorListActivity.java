package com.doctorsonrequest.savatarr;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.doctorsonrequest.savatarr.adapter.HomeListAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.doctorsonrequest.savatarr.SplashActivity.API_URL;

public class DoctorListActivity extends AppCompatActivity implements View.OnClickListener {
    private RecyclerView recycler_doctor;
    private RecyclerView.LayoutManager layoutManager;
    private Context mCtx;
    private ProgressBar progressBar4;
    private TextView nav_text;
    private ImageView btn_back2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_list);
        mCtx = this;

        nav_text = findViewById(R.id.nav_text);
        btn_back2 = findViewById(R.id.btn_back2);
        progressBar4 = findViewById(R.id.progressBar4);
        recycler_doctor =findViewById(R.id.recycler_doctor);
        layoutManager = new LinearLayoutManager(mCtx,LinearLayoutManager.VERTICAL,false);
        recycler_doctor.setLayoutManager(layoutManager);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String specialty = extras.getString("specialty");
            nav_text.setText(specialty);
            //The key argument here must match that used in the other activity
            fetchAllDoctors(mCtx,specialty);
        }
        btn_back2.setOnClickListener(this);
    }
    public void fetchAllDoctors(Context context, String specialty) {
        RequestQueue queue = Volley.newRequestQueue(context);
        String url = API_URL+"fetchAllDocs.php";
        if(!specialty.equals("")){
            url += "?specialty="+specialty;
        }
        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        JSONArray jArray = null;
                        try {
                            jArray = new JSONArray(response);
                            JSONObject jsonObject;
                            Map<String, Map<String, String>> parentMap =
                                    new HashMap<String, Map<String, String>>();
                            for (int i = 0; i < jArray.length(); i++) {
                                jsonObject = jArray.getJSONObject(i);
                                Map<String, String> map = new HashMap<String, String>();
                                map.put("name", jsonObject.getString("name"));
                                String gender;
                                if(jsonObject.getString("gender").equals("1")){
                                    gender = "Male";
                                }
                                else{
                                    gender = "Female";
                                }
                                map.put("gender", gender);
                                map.put("specialty", jsonObject.getString("specialty"));
                                map.put("profile_pic", jsonObject.getString("profile_pic"));
                                map.put("description", jsonObject.getString("description"));
                                parentMap.put(jsonObject.getString("name"),map);
                            }
                            dataHandler dh = new dataHandler();
                            dh.set(parentMap);

                            HomeListAdapter homeListAdapter = new HomeListAdapter(mCtx,dh);
                            recycler_doctor.setAdapter(homeListAdapter);

                            progressBar4.setVisibility(View.GONE);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("test3","\"That didn't work!\"");
            }
        });

// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_back2:
                finish();
                onBackPressed();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}

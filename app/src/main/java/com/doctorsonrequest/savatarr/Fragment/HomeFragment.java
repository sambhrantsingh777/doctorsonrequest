package com.doctorsonrequest.savatarr.Fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.doctorsonrequest.savatarr.SearchActivity;
import com.doctorsonrequest.savatarr.adapter.CategoryAdapter;
import com.doctorsonrequest.savatarr.adapter.HomeListAdapter;

import com.doctorsonrequest.savatarr.adapter.SymptomsAdapter;
import com.doctorsonrequest.savatarr.dataHandler;
import com.doctorsonrequest.savatarr.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class HomeFragment extends Fragment {
    private RecyclerView recycler_home,recycler_category,recycler_symptoms;
    private RecyclerView.LayoutManager layoutManager,layoutManager2,layoutManager3;
    private Context mCtx;
    private ProgressBar progressBar3;
    private EditText autoCompleteTextView_search;
    private LinearLayout ll_TextView_search;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        mCtx = getContext();
        init(view);
        return view;
    }
    private void init(View view){
        fetchAllDoctors(mCtx);

        progressBar3 = view.findViewById(R.id.progressBar3);
        recycler_home =view.findViewById(R.id.recycler_home);
        layoutManager = new LinearLayoutManager(mCtx,LinearLayoutManager.VERTICAL,false);
        recycler_home.setLayoutManager(layoutManager);

        recycler_category = view.findViewById(R.id.recycler_category);
        layoutManager2 =new LinearLayoutManager(mCtx,LinearLayoutManager.HORIZONTAL,false);
        recycler_category.setHasFixedSize(true);
        recycler_category.setLayoutManager(layoutManager2);


        recycler_symptoms = view.findViewById(R.id.recycler_symptoms);
        layoutManager3 =new LinearLayoutManager(mCtx,LinearLayoutManager.HORIZONTAL,false);
        recycler_symptoms.setHasFixedSize(true);
        recycler_symptoms.setLayoutManager(layoutManager3);

        SymptomsAdapter symptomsAdapter = new SymptomsAdapter(mCtx);
        recycler_symptoms.setAdapter(symptomsAdapter);

        CategoryAdapter categoryAdapter = new CategoryAdapter(mCtx);
        recycler_category.setAdapter(categoryAdapter);

        autoCompleteTextView_search = view.findViewById(R.id.autoCompleteTextView_search);
        autoCompleteTextView_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getContext(), SearchActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });






    }
    public void fetchAllDoctors(Context context) {
        RequestQueue queue = Volley.newRequestQueue(context);
        String url ="https://justtripit.herokuapp.com/api/fetchAllDocs.php";

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        JSONArray jArray = null;
                        try {
                            jArray = new JSONArray(response);
                            JSONObject jsonObject;
                            Map<String, Map<String, String>> parentMap =
                                    new HashMap<String, Map<String, String>>();
//                            Log.d("test3","test1");
                            for (int i = 0; i < jArray.length(); i++) {
                                jsonObject = jArray.getJSONObject(i);
                                Map<String, String> map = new HashMap<String, String>();
                                map.put("name", jsonObject.getString("name"));
                                String gender;
                                if(jsonObject.getString("gender").equals("1")){
                                    gender = "Male";
                                }
                                else{
                                    gender = "Female";
                                }
                                map.put("gender", gender);
                                map.put("specialty", jsonObject.getString("specialty"));
                                map.put("profile_pic", jsonObject.getString("profile_pic"));
                                map.put("description", jsonObject.getString("description"));
                                parentMap.put(jsonObject.getString("name"),map);
                            }
                            dataHandler dh = new dataHandler();
                            dh.set(parentMap);

                            HomeListAdapter homeListAdapter = new HomeListAdapter(mCtx,dh);
                            recycler_home.setAdapter(homeListAdapter);

                            progressBar3.setVisibility(View.GONE);
                        } catch (JSONException e) {
                            e.printStackTrace();
//                            Log.d("test3","test3");
                        }

//                        Log.d("test3",response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("test3","\"That didn't work!\"");
            }
        });

// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }
}

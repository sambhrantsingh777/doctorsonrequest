package com.doctorsonrequest.savatarr.Fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.doctorsonrequest.savatarr.AppointmentActivity;

import com.doctorsonrequest.savatarr.Constants;
import com.doctorsonrequest.savatarr.R;
import com.doctorsonrequest.savatarr.utils.SnackbarManager;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.doctorsonrequest.savatarr.SplashActivity.API_URL;
import static com.doctorsonrequest.savatarr.SplashActivity.MyPREFERENCES;


public class ProfileFragment extends Fragment implements View.OnClickListener {
    private final String user_name;
    private ImageView go_back;
    private Button button_submit;
    private LinearLayout ll_edit_profile,ll_profile_detail;
    private TextView txt_edit_password,txt_edit_phone,txt_edit_email,txt_edit_name;
    private EditText ed_name,ed_email,ed_phone,ed_password;
    private TextView txt_name,txt_email,txt_phone,txt_password,txt_appointment;
    private RelativeLayout rl_name,rl_email,rl_phone,rl_password;
    private ProgressBar profile_progressBar;
    private String name,mobile,password,email;

    public ProfileFragment(String uname) {
        this.user_name = uname;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        init(view);
        return view;
    }
    public void init(View view){
        go_back = view.findViewById(R.id.go_back);
        button_submit = view.findViewById(R.id.button_submit);
        txt_edit_password = view.findViewById(R.id.txt_edit_password);
        txt_edit_phone= view.findViewById(R.id.txt_edit_phone);
        txt_edit_email = view.findViewById(R.id.txt_edit_email);
        txt_edit_name = view.findViewById(R.id.txt_edit_name);
        ed_name = view.findViewById(R.id.ed_name);
        ed_email= view.findViewById(R.id.ed_email);
        ed_phone = view.findViewById(R.id.ed_phone);
        ed_password= view.findViewById(R.id.ed_password);
        txt_name= view.findViewById(R.id.txt_name);
        txt_email= view.findViewById(R.id.txt_email);
        txt_phone = view.findViewById(R.id.txt_phone);
        txt_password = view.findViewById(R.id.txt_password);
        rl_name= view.findViewById(R.id.rl_name);
        rl_email= view.findViewById(R.id.rl_email);
        rl_phone= view.findViewById(R.id.rl_phone);
        rl_password = view.findViewById(R.id.rl_password);
        txt_appointment = view.findViewById(R.id.txt_appointment);
        profile_progressBar = view.findViewById(R.id.profile_progressBar);



        txt_appointment.setOnClickListener(this);
        txt_edit_name.setOnClickListener(this);
        txt_edit_email.setOnClickListener(this);
        txt_edit_phone.setOnClickListener(this);
        button_submit.setOnClickListener(this);
        txt_edit_password.setOnClickListener(this);

        fetchProfileInfo(getContext());

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.txt_edit_password:
                rl_password.setVisibility(View.GONE);
                ed_password.setVisibility(View.VISIBLE);

                break;
            case R.id.txt_edit_phone:
                rl_phone.setVisibility(View.GONE);
                ed_phone.setVisibility(View.VISIBLE);
//                ed_phone.setText(mobile);
                break;
            case R.id.txt_edit_email:
                rl_email.setVisibility(View.GONE);
                ed_email.setVisibility(View.VISIBLE);
//                ed_email.setText(email);
                break;
            case R.id.txt_edit_name:
                rl_name.setVisibility(View.GONE);
                ed_name.setVisibility(View.VISIBLE);
                ed_name.setText(name);
                break;
            case R.id.button_submit:
                rl_password.setVisibility(View.VISIBLE);
                ed_password.setVisibility(View.GONE);
                rl_phone.setVisibility(View.VISIBLE);
                ed_phone.setVisibility(View.GONE);
                rl_email.setVisibility(View.VISIBLE);
                ed_email.setVisibility(View.GONE);
                rl_name.setVisibility(View.VISIBLE);
                ed_name.setVisibility(View.GONE);
                editInfo(v, getContext(), String.valueOf(ed_name.getText()),String.valueOf(ed_email.getText()),String.valueOf(ed_phone.getText()),String.valueOf(ed_password.getText()));
                break;
            case R.id.txt_appointment:
                Intent intent = new Intent(getContext(), AppointmentActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;
        }
    }

    public void fetchProfileInfo(Context context) {
        RequestQueue queue = Volley.newRequestQueue(context);
        String url = API_URL + "getUserData.php";
        // Request a string response from the provided URL.
        StringRequest sr = new StringRequest(Request.Method.POST,url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                JSONObject jObject = null;
                JSONObject jData = null;
                try {
                    jObject = new JSONObject(response);
                    String status = jObject.getString("status");
                    String data = jObject.getString("data");
                    if(status.equals("Success")){
                        jData = new JSONObject(data);
                        name = jData.getString("name");
                        email = jData.getString("email");
                        mobile = jData.getString("mobile");
                        password = jData.getString("password");
                        txt_name.setText(name);
                        txt_email.setText(email);
                        txt_phone.setText(mobile);
                        profile_progressBar.setVisibility(View.GONE);
                    }
                }
                catch (JSONException e){
                    e.printStackTrace();
                    profile_progressBar.setVisibility(View.GONE);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("savatarr_errorOnCall", String.valueOf(error));
                profile_progressBar.setVisibility(View.GONE);
            }
        }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("username", user_name);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/x-www-form-urlencoded");
                return params;
            }
        };
        queue.add(sr);
    }

    public void editInfo(final View v, Context context, final String edname, final String edemail, final String edphone, final String edpassword) {
        RequestQueue queue = Volley.newRequestQueue(context);
        String url = API_URL + "editInfo.php";
        if(!edname.equals("")||!edemail.equals("")||!edphone.equals("")||!edpassword.equals("")){

            // Request a string response from the provided URL.
            StringRequest sr = new StringRequest(Request.Method.POST,url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    JSONObject jObject;
                    try {
                        jObject = new JSONObject(response);
                        String status = jObject.getString("status");
                        String mssg = jObject.getString("mssg");
                        if(status.equals("Success")){
                            Snackbar snackbar = Snackbar
                                    .make(v, "Profile Data updated!", Snackbar.LENGTH_LONG);
                            snackbar.show();
                            if(!edname.equals("")){
                                txt_name.setText(edname);
                            }
                            if(!edemail.equals("")) {
                                txt_email.setText(edemail);
                            }
                            if(!edphone.equals("")){
                                txt_phone.setText(edphone);
                            }

                        }
                        else{
                            Snackbar snackbar = Snackbar
                                    .make(v, mssg, Snackbar.LENGTH_LONG);
                            snackbar.show();
                        }
                    }
                    catch (JSONException e){
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d("savatarr_errorOnCall", String.valueOf(error));
                    Snackbar snackbar = Snackbar
                            .make(v, "Some error occurred while updating profile. Check your connection.", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
            }){
                @Override
                protected Map<String,String> getParams(){
                    Map<String,String> params = new HashMap<String, String>();
                    params.put("user_name", user_name);
                    params.put("name", edname);
                    params.put("email", edemail);
                    params.put("phone", edphone);
                    params.put("password", edpassword);
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String,String> params = new HashMap<String, String>();
                    params.put("Content-Type","application/x-www-form-urlencoded");
                    return params;
                }
            };
            queue.add(sr);
        }
        else{
            Snackbar snackbar = Snackbar
                    .make(v, "Nothing to update!", Snackbar.LENGTH_LONG);
            snackbar.show();
        }
    }
}

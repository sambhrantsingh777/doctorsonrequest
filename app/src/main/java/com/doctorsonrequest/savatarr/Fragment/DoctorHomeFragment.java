package com.doctorsonrequest.savatarr.Fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.doctorsonrequest.savatarr.adapter.DoctorHomeAdapter;
import com.doctorsonrequest.savatarr.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.doctorsonrequest.savatarr.SplashActivity.API_URL;
import static com.doctorsonrequest.savatarr.SplashActivity.MyPREFERENCES;


public class DoctorHomeFragment extends Fragment {
    private RecyclerView recycler_home_doc;
    private RecyclerView.LayoutManager layoutManager;
    private Context mCtx;

    private ArrayList<String> patient_name = new ArrayList<String>();
    private ArrayList<String> appointment_time = new ArrayList<String>();
    private ArrayList<String> gender = new ArrayList<String>();
    private ArrayList<String> age = new ArrayList<String>();
    private ArrayList<String> statusList = new ArrayList<String>();
    private ArrayList<String> diseaseList = new ArrayList<String>();
    private View v = getView();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_doctor_home, container, false);
        mCtx = getContext();
        v = view;
        SharedPreferences sharedpreferences = this.getActivity().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        String doc_name = sharedpreferences.getString("doc_name", "NULL");
        if(!doc_name.equals("NULL")){
            fetchAllAppointments(doc_name);
        }
        init(view);
        return view;
    }
    public void init(View view){
        recycler_home_doc = view.findViewById(R.id.recycler_home_doc);
        layoutManager = new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false);
        recycler_home_doc.setLayoutManager(layoutManager);
    }

    public void fetchAllAppointments(final String doc_name) {
        RequestQueue queue = Volley.newRequestQueue(mCtx);
        String url = API_URL + "fetchAllAppointments.php";
        // Request a string response from the provided URL.
        StringRequest sr = new StringRequest(Request.Method.POST,url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                JSONObject jObject = null;
                JSONArray jData = null;
                try {
                    ProgressBar pb = v.findViewById(R.id.progressBar2);
                    pb.setVisibility(View.GONE);
                    jObject = new JSONObject(response);
                    String status = jObject.getString("status");
                    String data = jObject.getString("data");
                    if(status.equals("Success")){
                        jData = new JSONArray(data);
                        Log.d("testt", String.valueOf(jData.length()));
                        if(jData.length()>0){
                            for (int i=0; i<jData.length();i++){
                                try {
                                    JSONObject jObj = new JSONObject();
                                    jObj = jData.getJSONObject(i);
                                    patient_name.add(jObj.getString("patient_name"));
                                    appointment_time.add(jObj.getString("appointment_time"));
                                    age.add(jObj.getString("age"));
                                    gender.add(jObj.getString("gender"));
                                    statusList.add(jObj.getString("status"));
                                    diseaseList.add(jObj.getString("disease"));
                                    DoctorHomeAdapter doctorHomeAdapter = new DoctorHomeAdapter(mCtx,patient_name,appointment_time,age,gender,statusList,diseaseList);
                                    recycler_home_doc.setAdapter(doctorHomeAdapter);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                }
                catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("savatarr_errorOnCall", String.valueOf(error));
//                profile_progressBar.setVisibility(View.GONE);
            }
        }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("doc_name", doc_name);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/x-www-form-urlencoded");
                return params;
            }
        };
        queue.add(sr);
    }
}

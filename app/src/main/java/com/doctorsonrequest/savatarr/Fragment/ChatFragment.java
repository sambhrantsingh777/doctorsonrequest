package com.doctorsonrequest.savatarr.Fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.cometchat.pro.core.AppSettings;
import com.cometchat.pro.core.CometChat;
import com.cometchat.pro.exceptions.CometChatException;
import com.cometchat.pro.models.User;
import com.doctorsonrequest.savatarr.R;
import com.doctorsonrequest.savatarr.adapter.ChatInboxAdapter;

import screen.unified.CometChatUnified;

public class ChatFragment extends Fragment {
    private final Context ctxt;
    private final String username;
    private RecyclerView recyclerView_message;
    private RecyclerView.LayoutManager layoutManager;

    public ChatFragment(Context applicationContext, String userName) {
        this.ctxt = applicationContext;
        this.username = userName;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_chat, container, false);
        init(view);
        return view;
    }
    public void init(View view){
        recyclerView_message = view.findViewById(R.id.recycler_message);

        layoutManager = new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false);
        recyclerView_message.setLayoutManager(layoutManager);

        ChatInboxAdapter chatInboxAdapter = new ChatInboxAdapter(getContext());
        recyclerView_message.setAdapter(chatInboxAdapter);


    }
}

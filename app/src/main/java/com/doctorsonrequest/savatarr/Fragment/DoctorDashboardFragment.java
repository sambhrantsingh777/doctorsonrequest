package com.doctorsonrequest.savatarr.Fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.doctorsonrequest.savatarr.R;
import com.doctorsonrequest.savatarr.adapter.PatientSearchAdapter;
import com.doctorsonrequest.savatarr.pojoModel.AppointmentCountBeanModel;
import com.doctorsonrequest.savatarr.pojoModel.PatientSearchBeanModel;
import com.doctorsonrequest.savatarr.utils.ResponseHandler;
import com.doctorsonrequest.savatarr.utils.ServiceWrapper;
import com.doctorsonrequest.savatarr.utils.SnackbarManager;
import com.google.gson.JsonObject;

import retrofit2.Call;

import static com.doctorsonrequest.savatarr.SplashActivity.MyPREFERENCES;


public class DoctorDashboardFragment extends Fragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_doctor_dashboard, container, false);
        return view;
    }
}

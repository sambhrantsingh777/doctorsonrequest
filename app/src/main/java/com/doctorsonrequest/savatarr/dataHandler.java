package com.doctorsonrequest.savatarr;

import java.util.HashMap;
import java.util.Map;

public class dataHandler {

    public Map<String, Map<String, String>> doctorsList =
            new HashMap<String, Map<String, String>>();

    public void set(Map<String, Map<String, String>> data) {
        doctorsList = data;
    }

    public Map<String, Map<String, String>> get(){
        return this.doctorsList;
    }

}

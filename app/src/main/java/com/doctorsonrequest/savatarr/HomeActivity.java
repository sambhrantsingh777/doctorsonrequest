package com.doctorsonrequest.savatarr;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.cometchat.pro.core.AppSettings;
import com.cometchat.pro.core.Call;
import com.cometchat.pro.core.CometChat;
import com.cometchat.pro.exceptions.CometChatException;
import com.cometchat.pro.models.User;
import com.doctorsonrequest.savatarr.Fragment.ChatFragment;
import com.doctorsonrequest.savatarr.Fragment.HomeFragment;
import com.doctorsonrequest.savatarr.Fragment.ProfileFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import screen.unified.CometChatUnified;

import static com.doctorsonrequest.savatarr.Constants.COMAT_APP_ID;
import static com.doctorsonrequest.savatarr.Constants.COMAT_AUTH_KEY;
import static com.doctorsonrequest.savatarr.Constants.COMAT_REGION;
import static com.doctorsonrequest.savatarr.SplashActivity.MyPREFERENCES;


public class HomeActivity extends AppCompatActivity {
    private FragmentManager fragmentManager;
    private FragmentTransaction fragmentTransaction;
    BottomNavigationView navigation;
    private int mMenuId;
    private String TAG = "TESTCHAT";



    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
//            currentPosition = item.getItemId();
            SharedPreferences sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            String user_name = sharedpreferences.getString("user_name", "NULL");
            String name = sharedpreferences.getString("doc_name", "NULL");
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    OpenFragment(new HomeFragment());
                    return true;
                case R.id.navigation_profile:
                    OpenFragment(new ProfileFragment(user_name));
                    return true;
                case R.id.navigation_chat:
                    GoToChat(name, user_name);
                    OpenFragment(new ChatFragment(getApplicationContext(), user_name));
                    return true;
            }
            return false;
        }

        private void GoToChat(String user_name, String UID) {
            User user = new User();
            user.setUid(UID); // Replace with the UID for the user to be created
            user.setName(user_name); // Replace with the name of the user

            CometChat.createUser(user, COMAT_AUTH_KEY, new CometChat.CallbackListener<User>() {
                @Override
                public void onSuccess(User user) {
                    Log.d("createUser", user.toString());
                }

                @Override
                public void onError(CometChatException e) {
                    Log.e("createUser", e.getMessage());
                }
            });

            if (CometChat.getLoggedInUser() == null) {
                CometChat.login(UID, COMAT_AUTH_KEY, new CometChat.CallbackListener<User>() {

                    private static final String TAG = "CHATTEST";

                    @Override
                    public void onSuccess(User user) {
                        Log.d(TAG, "Login Successful : " + user.toString());
                        startActivity(new Intent(HomeActivity.this, CometChatUnified.class));
                    }

                    @Override
                    public void onError(CometChatException e) {
                        Log.d(TAG, "Login failed with exception: " + e.getMessage());
                    }
                });
            } else {
                Log.d("TESTCHAT", "Logged in Already : ");
                startActivity(new Intent(HomeActivity.this, CometChatUnified.class));
                // User already logged in
            }
            String listenerId = "HomeTest";

            CometChat.addCallListener(listenerId, new CometChat.CallListener() {

                @Override
                public void onIncomingCallReceived(Call call) {
                    Log.d(TAG, "Incoming call: " +
                            call.toString());
                }
                @Override
                public void onOutgoingCallAccepted(Call call) {
                    Log.d(TAG, "Outgoing call accepted: " +
                            call.toString());
                }
                @Override
                public void onOutgoingCallRejected(Call call) {
                    Log.d(TAG, "Outgoing call rejected: " +
                            call.toString());
                }
                @Override
                public void onIncomingCallCancelled(Call call) {
                    Log.d(TAG, "Incoming call cancelled: " +
                            call.toString());
                }

            });
//            String sessionID = "SESSION_ID";
//
//            CometChat.acceptCall(sessionID, new CometChat.CallbackListener<Call>() {
//                @Override
//                public void onSuccess(Call call) {
//                    Log.d(TAG, "Call accepted successfully: " + call.toString());
//                }
//                public void onError(CometChatException e) {
//                    Log.d(TAG, "Call acceptance failed with exception: " + e.getMessage());
//                }
//            });
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        AppSettings appSettings=new AppSettings.AppSettingsBuilder().subscribePresenceForAllUsers().setRegion(COMAT_REGION).build();
        CometChat.init(getApplicationContext(), COMAT_APP_ID,appSettings, new CometChat.CallbackListener<String>() {
            private String TAG = "CHATTEST";

            @Override
            public void onSuccess(String successMessage) {
                Log.d(TAG, "Initialization completed successfully");
            }

            @Override
            public void onError(CometChatException e) {
                Log.d(TAG, "Initialization failed with exception: " + e.getMessage());
            }
        });

        navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        OpenFragment(new HomeFragment());


    }

    public void OpenFragment(Fragment fragment)
    {
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment_host_container, fragment);
        fragmentTransaction.commit();
    }

    public void logout(View view){

        try {
            CometChat.logout(new CometChat.CallbackListener<String>() {
                private static final String TAG = "CHATTEST";

                @Override
                public void onSuccess(String successMessage) {
                    Log.d(TAG, "Logout completed successfully");
                }

                @Override
                public void onError(CometChatException e) {
                    Log.d(TAG, "Logout failed with exception: " + e.getMessage());
                }
            });
        } finally {
            Log.d("savatarr_logout","loggedOUt");
            SharedPreferences sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.remove("isLoggedIn").commit();
            editor.remove("user_name").commit();
            editor.remove("position").commit();
            try{
                editor.remove("doc_name").commit();
            } catch (Exception e) {
                e.printStackTrace();
            }
            Toast.makeText(this, "Logged Out!", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(getBaseContext(),LoginActivity.class));
            finish();
        }
    }

}

package com.doctorsonrequest.savatarr;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class PatientDetailActivity extends AppCompatActivity {
    public String patient,age,disease,status,appointment_time,symptoms,severity;
    private TextView p_name,p_age,p_disease,p_pay_status,p_symptoms,p_severity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_detail);

        patient = getIntent().getStringExtra("patient");
        age = getIntent().getStringExtra("age");
        disease = getIntent().getStringExtra("disease");
        status = getIntent().getStringExtra("status");
        appointment_time = getIntent().getStringExtra("appointment_time");
        symptoms = getIntent().getStringExtra("symptoms");
        severity = getIntent().getStringExtra("severity");

        p_name = findViewById(R.id.p_name);
        p_age = findViewById(R.id.p_age);
        p_disease = findViewById(R.id.p_disease);
        p_pay_status = findViewById(R.id.p_pay_status);
        p_symptoms = findViewById(R.id.symptoms);
        p_severity = findViewById(R.id.severity);

        String st = null;
        if (status.equals("1")) {
            st = "Confirm";
        } else if (status.equals("0")) {
            st = "Pending Confirmation";
        } else {
            st = "Pending Confirmation";
        }

        p_name.setText(patient);
        p_age.setText(age);
        p_disease.setText(disease);
        p_pay_status.setText(st);
        p_severity.setText(severity);
        p_symptoms.setText(symptoms);
    }

    public void goBack(View v){
        finish();
    }
}

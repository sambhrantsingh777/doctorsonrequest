package com.doctorsonrequest.savatarr;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;

import com.cometchat.pro.core.AppSettings;
import com.cometchat.pro.core.Call;
import com.cometchat.pro.core.CometChat;
import com.cometchat.pro.exceptions.CometChatException;
import com.cometchat.pro.models.User;
import com.doctorsonrequest.savatarr.Fragment.ChatFragment;
import com.doctorsonrequest.savatarr.Fragment.DoctorDashboardFragment;
import com.doctorsonrequest.savatarr.Fragment.DoctorHomeFragment;
import com.doctorsonrequest.savatarr.R;
import com.doctorsonrequest.savatarr.pojoModel.AppointmentCountBeanModel;
import com.doctorsonrequest.savatarr.utils.ResponseHandler;
import com.doctorsonrequest.savatarr.utils.ServiceWrapper;
import com.doctorsonrequest.savatarr.utils.SnackbarManager;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.gson.JsonObject;

import screen.unified.CometChatUnified;

import static com.doctorsonrequest.savatarr.Constants.COMAT_APP_ID;
import static com.doctorsonrequest.savatarr.Constants.COMAT_AUTH_KEY;
import static com.doctorsonrequest.savatarr.Constants.COMAT_REGION;
import static com.doctorsonrequest.savatarr.SplashActivity.MyPREFERENCES;

public class DoctorHomeActivity extends AppCompatActivity {
    private FragmentManager fragmentManager;
    private FragmentTransaction fragmentTransaction;
    BottomNavigationView navigation;
    private int mMenuId;
    private Toolbar toolbar;
    private AutoCompleteTextView autoCompleteTextView_search;
    private Context mCtx;


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            SharedPreferences sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            String user_name = sharedpreferences.getString("user_name", "NULL");
            String doc_name = sharedpreferences.getString("doc_name", "NULL");
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    OpenFragment(new DoctorHomeFragment());
                    toolbar.setVisibility(View.VISIBLE);
                    return true;
                case R.id.navigation_dashboard:
                    JsonObject jsonObject = new JsonObject();
                    jsonObject.addProperty("doc_name",doc_name);
                    ServiceWrapper serviceWrapper = new ServiceWrapper(mCtx);
                    retrofit2.Call<AppointmentCountBeanModel> call = serviceWrapper.getService().getAptCount(doc_name);
                    serviceWrapper.ReqHandlerDoctor(call,  new ResponseHandler<AppointmentCountBeanModel>(mCtx) {
                        @Override
                        public void onStarted() {

                        }

                        @Override
                        public void onResponse(AppointmentCountBeanModel response) {
                            if(response.getStatusCode().equals("200")) {
                                TextView month = findViewById(R.id.thisMonth);
                                TextView week = findViewById(R.id.thisWeek);
                                TextView total = findViewById(R.id.total);
                                try {
                                    month.setText(response.getData().getMonthly());
                                    week.setText(response.getData().getWeekly());
                                    total.setText(response.getData().getTotal());
                                } catch (Exception e) {
                                    Log.d("testt", "exception on count");
                                }
                            }
                                else {
                                SnackbarManager.sException(mCtx,"No Results!");
                            }
                        }
                    });
                    OpenFragment(new DoctorDashboardFragment());
                    toolbar.setVisibility(View.GONE);
                    return true;
                case R.id.navigation_chat:
                    GoToChat(doc_name, user_name);
                    OpenFragment(new ChatFragment(getApplicationContext(), user_name));
                    toolbar.setVisibility(View.GONE);
                    return true;
            }
            return false;
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AppSettings appSettings=new AppSettings.AppSettingsBuilder().subscribePresenceForAllUsers().setRegion(COMAT_REGION).build();
        CometChat.init(getApplicationContext(), COMAT_APP_ID,appSettings, new CometChat.CallbackListener<String>() {
            private String TAG = "CHATTEST";

            @Override
            public void onSuccess(String successMessage) {
                Log.d(TAG, "Initialization completed successfully");
            }

            @Override
            public void onError(CometChatException e) {
                Log.d(TAG, "Initialization failed with exception: " + e.getMessage());
            }
        });
        mCtx = this;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_home);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
//        navigation.getMenu().findItem(R.id.navigation_home).setEnabled(true);

        OpenFragment(new DoctorHomeFragment());
        autoCompleteTextView_search = findViewById(R.id.autoCompleteTextView_search);

        autoCompleteTextView_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mCtx, SearchActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                startActivity(new Intent(getBaseContext(),LoginActivity.class));
                finish();
                break;
            case R.id.account:
                startActivity(new Intent(getBaseContext(),DoctorProfileActivity.class));
                break;
        }
        return super.onOptionsItemSelected(item);
    }
    public void OpenFragment(Fragment fragment)
    {
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment_host_container, fragment);
        fragmentTransaction.commit();
    }

    private void GoToChat(String user_name, String UID) {
        User user = new User();
        user.setUid(UID); // Replace with the UID for the user to be created
        user.setName(user_name); // Replace with the name of the user

        CometChat.createUser(user, COMAT_AUTH_KEY, new CometChat.CallbackListener<User>() {
            @Override
            public void onSuccess(User user) {
                Log.d("createUser", user.toString());
            }

            @Override
            public void onError(CometChatException e) {
                Log.e("createUser", e.getMessage());
            }
        });

        if (CometChat.getLoggedInUser() == null) {
            CometChat.login(UID, COMAT_AUTH_KEY, new CometChat.CallbackListener<User>() {

                private static final String TAG = "CHATTEST";

                @Override
                public void onSuccess(User user) {
                    Log.d(TAG, "Login Successful : " + user.toString());
                    startActivity(new Intent(DoctorHomeActivity.this, CometChatUnified.class));
                }

                @Override
                public void onError(CometChatException e) {
                    Log.d(TAG, "Login failed with exception: " + e.getMessage());
                }
            });
        } else {
            Log.d("TESTCHAT", "Logged in Already : ");
            startActivity(new Intent(DoctorHomeActivity.this, CometChatUnified.class));
            // User already logged in
        }
        String listenerId = "HomeTest";

        CometChat.addCallListener(listenerId, new CometChat.CallListener() {

            private static final String TAG = "CHATTEST";
            @Override
            public void onIncomingCallReceived(Call call) {
                Log.d(TAG, "Incoming call: " +
                        call.toString());
            }
            @Override
            public void onOutgoingCallAccepted(Call call) {
                Log.d(TAG, "Outgoing call accepted: " +
                        call.toString());
            }
            @Override
            public void onOutgoingCallRejected(Call call) {
                Log.d(TAG, "Outgoing call rejected: " +
                        call.toString());
            }
            @Override
            public void onIncomingCallCancelled(Call call) {
                Log.d(TAG, "Incoming call cancelled: " +
                        call.toString());
            }

        });
//            String sessionID = "SESSION_ID";
//
//            CometChat.acceptCall(sessionID, new CometChat.CallbackListener<Call>() {
//                @Override
//                public void onSuccess(Call call) {
//                    Log.d(TAG, "Call accepted successfully: " + call.toString());
//                }
//                public void onError(CometChatException e) {
//                    Log.d(TAG, "Call acceptance failed with exception: " + e.getMessage());
//                }
//            });
    }
}

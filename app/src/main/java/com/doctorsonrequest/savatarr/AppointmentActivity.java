package com.doctorsonrequest.savatarr;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.doctorsonrequest.savatarr.adapter.ExecuteAppointmentAdapter;
import com.doctorsonrequest.savatarr.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.doctorsonrequest.savatarr.SplashActivity.API_URL;
import static com.doctorsonrequest.savatarr.SplashActivity.MyPREFERENCES;

public class AppointmentActivity extends AppCompatActivity implements View.OnClickListener {
    private ImageView btn_back1;
    private RecyclerView recycler_appointments;
    private RecyclerView.LayoutManager layoutManager;
    private Context mCtx;
    ArrayList<String> doctor_assigned = new ArrayList<String>();
    ArrayList<String> appointment_time = new ArrayList<String>();
    ArrayList<String> statusList = new ArrayList<String>();
    ArrayList<String> profile_pic = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mCtx = this;
        fetchAllAppointments();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appointment);
        btn_back1 = findViewById(R.id.btn_back1);

        recycler_appointments = findViewById(R.id.recycler_appointments);
        layoutManager = new LinearLayoutManager(mCtx,LinearLayoutManager.VERTICAL,false);
        recycler_appointments.setLayoutManager(layoutManager);

        btn_back1.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_back1:
                finish();
                onBackPressed();
                break;
        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public void fetchAllAppointments() {
        RequestQueue queue = Volley.newRequestQueue(mCtx);
        String url = API_URL + "fetchAllAppointments.php";
        // Request a string response from the provided URL.
        StringRequest sr = new StringRequest(Request.Method.POST,url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                ProgressBar pb = findViewById(R.id.progressBar7);
                pb.setVisibility(View.GONE);
                JSONObject jObject = null;
                JSONArray jData = null;
                try {
                    jObject = new JSONObject(response);
                    String status = jObject.getString("status");
                    String data = jObject.getString("data");
                    if(status.equals("Success")){
//                        Log.d("testt",data);
                        jData = new JSONArray(data);
                        for (int i=0; i<jData.length();i++){
                            try {
                                JSONObject jObj = new JSONObject();
                                jObj = jData.getJSONObject(i);
                                doctor_assigned.add(jObj.getString("doctor_assigned"));
                                appointment_time.add(jObj.getString("appointment_time"));
                                statusList.add(jObj.getString("status"));
                                profile_pic.add(jObj.getString("profile_pic"));
                                ExecuteAppointmentAdapter appointmentAdapter = new ExecuteAppointmentAdapter(mCtx , doctor_assigned, appointment_time, statusList, profile_pic);
                                recycler_appointments.setAdapter(appointmentAdapter);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
                catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("savatarr_errorOnCall", String.valueOf(error));
//                profile_progressBar.setVisibility(View.GONE);
            }
        }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                SharedPreferences sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                String user_name = sharedpreferences.getString("user_name", "NULL");
                params.put("user_name", user_name);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/x-www-form-urlencoded");
                return params;
            }
        };
        queue.add(sr);
    }
}

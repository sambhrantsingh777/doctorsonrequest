package com.doctorsonrequest.savatarr;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.doctorsonrequest.savatarr.R;

public class AppointmentConfirmActivity extends AppCompatActivity {
    private String doctor_name,date;
    private TextView txt_doctor_name,txt_date;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appointment_confirm);
        doctor_name = getIntent().getStringExtra("drName");
        date = getIntent().getStringExtra("date");

        txt_doctor_name = findViewById(R.id.txt_doctor_name);
        txt_date = findViewById(R.id.txt_date);

        txt_doctor_name.setText(doctor_name);
        txt_date.setText(date);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(getApplicationContext(),HomeActivity.class));
        finishAffinity();
    }
}

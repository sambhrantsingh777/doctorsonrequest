package com.doctorsonrequest.savatarr;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.doctorsonrequest.savatarr.R;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONException;
import org.json.JSONObject;

import static com.doctorsonrequest.savatarr.SplashActivity.API_URL;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    private TextView signup;
    private Button login;
    public static final String MyPREFERENCES = "userData" ;
    private ProgressBar pb_login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        signup = findViewById(R.id.text_signup);
        login = findViewById(R.id.button_login);
        pb_login = findViewById(R.id.pb_login);

        String text = signup.getText().toString();

        SpannableString ss = new SpannableString(text);

        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), SignUpActivity.class);
                startActivity(intent);

            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(getResources().getColor(R.color.colorPrimary));
                ds.setUnderlineText(false);
            }
        };

        ss.setSpan(clickableSpan, 19, 30,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        signup.setText(ss);
        signup.setMovementMethod(LinkMovementMethod.getInstance());
        signup.setHighlightColor(Color.TRANSPARENT);


        login.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_login:
                String check = validate();
                if(check.equals("valid")){
                    pb_login.setVisibility(View.VISIBLE);
                    login(v);
                }
                else{
                    Snackbar snackbar = Snackbar
                            .make(v, check, Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
                break;
        }

    }

    public  void  login(final View v) {
        TextInputEditText username = (TextInputEditText)findViewById(R.id.login_email);
        TextInputEditText password = (TextInputEditText)findViewById(R.id.login_pwd);
        RequestQueue queue = Volley.newRequestQueue(this);
        final String user_name = String.valueOf(username.getText());
        String url = API_URL + "login.php?username="+user_name+"&password="+String.valueOf(password.getText());

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        JSONObject jObject = null;
                        try {
                            jObject = new JSONObject(response);
                            String status = jObject.getString("status");
                            String mssg = jObject.getString("mssg");
                            String position = jObject.getString("position");
                            if(status.equals("Success")){
                                startActivity(new Intent(getApplicationContext(),(position.equals("DOC")) ? DoctorHomeActivity.class:HomeActivity.class));
                                Toast.makeText(getApplicationContext(), mssg,
                                        Toast.LENGTH_LONG).show();
                                SharedPreferences sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = sharedpreferences.edit();
                                editor.putString("isLoggedIn", "Yes");
                                editor.putString("user_name", user_name);
                                editor.putString("position", position);
                                if(position.equals("DOC")){
                                    editor.putString("doc_name", mssg.split("Welcome! ")[1]);
                                }
                                editor.commit();
                                pb_login.setVisibility(View.GONE);
                                finishAffinity();
                            }
                            else{
                                pb_login.setVisibility(View.GONE);
                                Snackbar snackbar = Snackbar
                                        .make(v, mssg, Snackbar.LENGTH_LONG);
                                snackbar.show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Snackbar snackbar = Snackbar
                                    .make(v, "Could not log you in! Check Connection.", Snackbar.LENGTH_LONG);
                            pb_login.setVisibility(View.GONE);
                            snackbar.show();
                        }

//                        Log.d("test3",response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Snackbar snackbar = Snackbar
                        .make(v, "Could not sign up! Server not responding.", Snackbar.LENGTH_LONG);
                pb_login.setVisibility(View.GONE);
                snackbar.show();
//                Log.d("test3","\"That didn't work!\"");
            }
        });

// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    public String validate(){
        TextInputEditText username = (TextInputEditText)findViewById(R.id.login_email);
        TextInputEditText password = (TextInputEditText)findViewById(R.id.login_pwd);
        if(String.valueOf(username.getText()).equals("")){
            return "Username cannot be blank!";
        }
        if(String.valueOf(password.getText()).equals("")){
            return "Password cannot be blank!";
        }
        return "valid";
    }
}

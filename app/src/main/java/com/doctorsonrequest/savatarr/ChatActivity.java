package com.doctorsonrequest.savatarr;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.doctorsonrequest.savatarr.adapter.ChatAdapter;
import com.doctorsonrequest.savatarr.R;

public class ChatActivity extends AppCompatActivity implements View.OnClickListener {
    private ImageView btn_back3;
    private RecyclerView recyclerView_chat;
    private RecyclerView.LayoutManager layoutManager;
    private Context mCtx;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        btn_back3 = findViewById(R.id.btn_back3);

        recyclerView_chat = findViewById(R.id.recycler_chat);
        layoutManager = new LinearLayoutManager(mCtx,LinearLayoutManager.VERTICAL,false);
        recyclerView_chat.setLayoutManager(layoutManager);

        ChatAdapter chatAdapter = new ChatAdapter(mCtx);
        recyclerView_chat.setAdapter(chatAdapter);



        btn_back3.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_back3:
                finish();
                onBackPressed();
                break;
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}

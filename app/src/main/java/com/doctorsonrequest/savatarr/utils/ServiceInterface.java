package com.doctorsonrequest.savatarr.utils;


import com.doctorsonrequest.savatarr.pojoModel.AppointmentCountBeanModel;
import com.doctorsonrequest.savatarr.pojoModel.KeysBeanModel;
import com.doctorsonrequest.savatarr.pojoModel.PatientSearchBeanModel;
import com.doctorsonrequest.savatarr.pojoModel.SearchBeanModel;
import com.google.gson.JsonObject;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;


public interface ServiceInterface {

//    @GET("location/countries")
//    Call<CountryBeanModel>  getCountryName();


    @FormUrlEncoded
    @POST("fetchAllDoctors.php")
    Call<SearchBeanModel> getSearch(@Field("search") String search);

    @FormUrlEncoded
    @POST("patientSearch.php")
    Call<PatientSearchBeanModel> getSearchPatient(@Field("doc_name") String doc_name,
                                                  @Field("keyword") String keyword);

    @GET("getConstants.php")
    Call<KeysBeanModel>  getKey();

    @FormUrlEncoded
    @POST("getApptCounts.php")
    Call<AppointmentCountBeanModel>  getAptCount(@Field("doc_name") String doc_name);

}

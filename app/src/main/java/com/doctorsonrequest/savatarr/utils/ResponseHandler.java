package com.doctorsonrequest.savatarr.utils;

import android.content.Context;



public abstract class ResponseHandler<T> {
    Context mContext;
    public ResponseHandler(Context context){
        super();
        this.mContext = context;
    }
    public abstract void onStarted();
    public abstract void onResponse(final T response);


    public void onError(final String errorResponse) {
        SnackbarManager.sException(mContext,errorResponse);
    }

    public void onFailure(Throwable throwable) {
        SnackbarManager.sException(mContext,throwable.getMessage());
    }

    public void  onInternetUnavailable(){
        SnackbarManager.sNoInternet(mContext,"Make sure you have an active data connection");
    }
    public void  onInternete(){
        SnackbarManager.sNoInternet(mContext,"Make sure you have an active data connection");
    }

}

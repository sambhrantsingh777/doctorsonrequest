package com.doctorsonrequest.savatarr;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import android.view.View;
import android.widget.TextView;


import com.doctorsonrequest.savatarr.adapter.PatientSearchAdapter;
import com.doctorsonrequest.savatarr.pojoModel.KeysBeanModel;
import com.doctorsonrequest.savatarr.pojoModel.PatientSearchBeanModel;
import com.doctorsonrequest.savatarr.utils.PreferenceManager;
import com.doctorsonrequest.savatarr.utils.ResponseHandler;
import com.doctorsonrequest.savatarr.utils.ServiceWrapper;
import com.doctorsonrequest.savatarr.utils.SnackbarManager;
import com.google.gson.JsonObject;

import retrofit2.Call;

public class SplashActivity extends AppCompatActivity {
    private static int SPLASH_SCREEN_TIME_OUT=2000;
    private Context mCtx;
    private TextView txt_start;
    public static final String MyPREFERENCES = "userData" ;
    public static String API_URL = "https://justtripit.herokuapp.com/api/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        final String isFirstOpen = sharedpreferences.getString("isFirstOpen", "Yes");
        String isLoggedIn = sharedpreferences.getString("isLoggedIn", "No");
        final String position = sharedpreferences.getString("position", "PAT");
        setContentView(R.layout.activity_main);
        txt_start = findViewById(R.id.txt_start);
        mCtx =this;
        getKey();
        if(isFirstOpen.equals("Yes")){
            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.putString("isFirstOpen", "No");
            editor.commit();

            if(isLoggedIn.equals("Yes")){
                txt_start.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(new Intent(mCtx,(position.equals("DOC")) ? DoctorHomeActivity.class:HomeActivity.class));
                        finish();
                    }
                });
            }
            else{
                txt_start.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(new Intent(mCtx,SignUpActivity.class));
                        finish();
                    }
                });
            }
        }
        else{
            txt_start.setVisibility(View.GONE);
            if(isLoggedIn.equals("Yes")){
                new Handler().postDelayed(new Runnable(){
                    @Override
                    public void run() {
                        /* Create an Intent that will start the Menu-Activity. */
                        startActivity(new Intent(mCtx,(position.equals("DOC")) ? DoctorHomeActivity.class:HomeActivity.class));
                        finish();
                    }
                }, SPLASH_SCREEN_TIME_OUT);
            }
            else{
                new Handler().postDelayed(new Runnable(){
                    @Override
                    public void run() {
                        /* Create an Intent that will start the Menu-Activity. */
                        startActivity(new Intent(mCtx,LoginActivity.class));
                        finish();
                    }
                }, SPLASH_SCREEN_TIME_OUT);
            }
        }
    }
    private void getKey(){
        ServiceWrapper serviceWrapper = new ServiceWrapper(mCtx);
        Call<KeysBeanModel> call = serviceWrapper.getService().getKey();
        serviceWrapper.ReqHandlerDoctor(call,  new ResponseHandler<KeysBeanModel>(mCtx) {
            @Override
            public void onStarted() {

            }

            @Override
            public void onResponse(KeysBeanModel response) {
                if(response.getStatusCode().equals("200")) {
                    Constants.RAZOR_KEY = response.getData().getRazorKey();
                    Constants.COMAT_APP_ID = response.getData().getComatAppId();
                    Constants.COMAT_AUTH_KEY = response.getData().getComatAuthKey();
                    Constants.COMAT_REGION = response.getData().getComatRegion();
//                    PreferenceManager.saveString(mCtx,"razor_key",response.getData().getRazorKey());
//                    PreferenceManager.saveString(mCtx,"comat_app_id",response.getData().getComatAppId());
//                    PreferenceManager.saveString(mCtx,"comat_auth_key",response.getData().getComatAuthKey());
//                    PreferenceManager.saveString(mCtx,"comat_region",response.getData().getComatRegion());

                }else {
                    SnackbarManager.sException(mCtx,response.getMssg());
                }

            }
        });
    }
}
